<%-- 
    Document   : login
    Created on : 12-jun-2016, 12:14:23
    Author     : Damián
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Iniciar Sesion</title>
    </head>
    <body>
        <div class="main ">
            <section class="formulario">
                <article class="row">

                    <div class="media">
                        <div class="media-left media-middle col-md-5">
                            <a href="#">
                                <img class="media-object img-responsive" src="app/images/mega.png"  alt="Mega Global Comunicación">
                            </a>
                        </div>
                        <div class="media-body">
                            <form name="formulario" class="col-md-11 col-md-offset-1">
                                <h3>Iniciar Sesi&oacute;n</h3>
                                <div class="form-group">
                                    <label for="user">Usuario</label>
                                    <input class="form-control" id="user" placeholder="Ingrese usuario" type="text" required="">
                                </div>
                                <div class="form-group">
                                    <label for="password" >Contrase&ntilde;a</label>
                                    <input required="" class="form-control" id="password" placeholder="Ingrese su contraseña" type="password" onkeypress=" if (event.keyCode == 13) {
                                                acceso.acceder()
                                            }">
                                    <span id="help" class="help-block"></span>
                                </div>
                                <div class="form-group">
                                    <label>
                                        <input type="checkbox" value="">&nbsp;Mantener sesi&oacute;n iniciada
                                    </label>
                                </div>
                                <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                    <div class="btn-group" role="group">
                                        <button class="btn btn-default" type="button" onclick="acceso.establecerCuenta()">Establecer cuenta</button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button class="btn btn-success" type="button" onclick="acceso.acceder()">Acceder</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </article>

                <div id="spPassword" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Establecer cuenta</h4>
                            </div>
                            <div class="modal-body">
                                <p>Para continuar debe ingresar el código que se le ha sido enviado al correo electrónico</p>
                                <div class="form-group">
                                    <input id="token_cod" type="text" class="form-control col-lg-12 text-center" placeholder="Escriba el código">
                                    <div class="help-block">
                                        <p class="text-warning">Para evitar errores, copie y pegue el código enviado, el mismo tiene validez por un tiempo limitado</p>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-default" data-dismiss="modal" title="Cancelar"><span class="glyphicon glyphicon-remove"></span></button>
                                <button class="btn btn-success" title="Aceptar" onclick="acceso.verificar_token()"><span class="glyphicon glyphicon-ok"></span></button>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </div>
        <script src="app/validator/vLogin.js" type="text/javascript"></script>
    </body>
</html>