<%-- 
    Document   : error
    Created on : 19-jun-2016, 18:44:36
    Author     : Damián
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    </head>
    <body>
        <div class="main ">
            <section class="formulario">
                <article class="row">

                    <fieldset>
                        <legend>Ha ocurrido un error</legend>
                        <p>
                            La pagina solicitada no puede mostrarse.
                        </p>


                    </fieldset>
                </article>

            </section>
            <div class="btn-group botones_operativos">
                <button type="button" class="btn btn-success col-lg-12" onclick="window.open('/megaglobal_web_2/', '_self')">Regresar al menú principal <span class="badge"> <span class="icon-home"></span> </span></button>
            </div>
        </div>

    </body>
</html>
