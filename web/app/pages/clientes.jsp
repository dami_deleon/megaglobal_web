<%-- 
    Document   : clientes
    Created on : 27-oct-2015, 19:13:20
    Author     : Damián
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%
            /*
            
             */
        %>
    </head>
    <body>
        <!--	cli_cod, cli_nombre, cli_ci_ruc, cli_direccion_1, cli_tel_1, cli_email_1, ciu_cod, nac_cod -->
        <section class="main" >
            <div class="btn-group-lg botones_operativos">
                <button id="new" class="btn btn-success" onclick="clientes.new()"><span class="glyphicon glyphicon-plus"></span></button>
                <button id="report_ord" class="btn btn-warning" onclick="clientes.view()"><span class="glyphicon glyphicon-list-alt"></span></button>
                <button class="salida btn btn-danger" onclick="clientes.salir()"><span class="glyphicon glyphicon-remove"></span></button>
            </div>
            <section class="formulario">

                <script id="t_title" type="text/x-handlebars-template">
                    {{ title }}
                </script>
                <article class="row" id="formu" style="display: none">
                    <h2 id="title"></h2>
                    <div class="form-group col-md-6">
                        <label for="cli_nombre">Nombre / Razon Social</label>
                        <input id="cli_nombre" type="text" class="form-control">
                    </div>
                    <div class="form-group col-md-6 has-feedback">
                        <label for="cli_ci_ruc">CI / RUC</label>
                        <input id="cli_ci_ruc" type="text" class="form-control" onchange="clientes.verifica_ruc_ci()">
                        <span class="glyphicon glyphicon-warning-sign form-control-feedback warning" id="wCli_ci_ruc" style="display: none"></span>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Direccion</label>
                        <input id="cli_direccion" type="text" class="form-control">
                    </div>
                    <script id="t_ciudades" type="text/x-handlebars-template">
                        {{#if ciudad}}
                            <option value="0">Seleccione Ciudad</option>
                        {{#each ciudad}}
                            <option value="{{ciu_cod}}">{{ciu_descri}}</option>
                        {{/each}}
                        {{else}}
                            <option value="0">No se ha recuperado ninguna ciudad</option>
                        {{/if}}
                    </script>

                    <div class="form-group col-md-6">
                        <label>Ciudad</label>
                        <select name="" id="ciu_cod" class="selectpicker form-control" data-live-search="true">
                            <option value="0">Cargando ciudades...</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Telefono</label>
                        <input id="cli_telefono" type="text" class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                        <label>Correo Electr&oacute;nico</label>
                        <input id="cli_email" type="text" class="form-control">
                    </div>

                </article>

                <article class="row" id="info">
                    <h2 >Gestionar Clientes</h2>
                    <table class="table table-bordered table-condensed table-hover data table-primary"  id="clientes" data-height="5">
                        <thead>
                            <tr>
                                <td>Código</td>
                                <td>Razón Social</td>
                                <td>RUC o CI N&ordm;</td>
                                <td>Telefono</td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <script id="t_clientes" type="text/x-handlebars-template">
                        {{#if data}}
                        {{#each data}}                            
                            <tr class="clickable-row b" data-index="{{@index}}">
                                <td>{{ cli_cod }}</td>
                                <td>{{ cli_nombre }}</td>
                                <td>{{ cli_ci_ruc }}</td>
                                <td>{{ cli_telefono }}</div>
                                </span>
                                </td>
                        </tr>
                        {{/each}}
                        {{else}}
                        <tr>
                            <td colspan="4">Se ha producido un error al cargar los clientes</td>
                            <td></td>
                        </tr>
                        {{/if}}
                    </script>
                </article>
                <div id="info_clientes"></div>
            </section>
            <div class="btn-group-lg text-right botones_operativos abajo">
                <button id="save" class="btn btn-success" onclick="clientes.save()"><span class="glyphicon glyphicon-ok"></span></button>
                <button id="save_new" class="btn btn-warning" onclick="clientes.save_new()"><span class="glyphicon glyphicon-ok"></span></button>
                <button class="btn btn-default" id="cancel" onclick="clientes.cancel()"><span class="glyphicon glyphicon-remove"></span></button>
            </div>
        </section>
        <script src="app/validator/vClientes.js" type="text/javascript"></script>
    </body>
</html>
