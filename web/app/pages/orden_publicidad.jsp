<%-- 
    Document   : orden_publicidad
    Created on : 31/05/2015, 04:33:43 PM
    Author     : Damián
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Orden de publicidad | Mega Global Comunicaci&oacute;n</title>

    </head>
    <body>
        <div class="main">
            <div class="botones_operativos btn-group-lg">
                <button class="btn btn-danger" onclick="orden.exit()"><span class="glyphicon glyphicon-remove"></span></button>
                <button class="btn btn-warning" id="op-view" onclick="orden.recuperaOrden()"><span class="glyphicon glyphicon-list"></span></button>
                <button class="btn btn-success" id="op-new" onclick="orden.new()"><span class="glyphicon glyphicon-plus"></span></button>
                <button class="btn btn-default" id="op-print" onclick="orden.print()" style="display: none"><span class="glyphicon glyphicon-print"></span></button>
            </div>

            <div class="formulario">
                <article class="row" id="op-presentacion">
                    <div class="jumbotron">
                        <h1>Órdenes De Publicidad</h1>
                        <p>Para generar la Orden de Publicidad debes dar clic en el botón
                            <a class="btn btn-success"> <span class="glyphicon glyphicon-plus"></span></a> 
                            e indicar el Número de la Propuesta emitida.
                        </p>
                        <p>Para visualizar una orden emitida dar clic en el botón
                            <a class="btn btn-warning"> <span class="glyphicon glyphicon-list"></span></a> 
                            e indicar el Número de la Orden de Publicidad.
                        </p>
                    </div>
                </article>
                <article class="row" id="op-cabecera" style="display: none">
                    <h2> Ordenes de Publicidad</h2>
                    <div class="form-group col-md-6">
                        <label for="ord_titulo" class="control-label">Titulo de la Orden</label>
                        <input type="text" class="form-control" id="ord_titulo"/>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="ord_fecha" class="control-label">Fecha</label>
                        <input type="text" class="form-control" id="ord_fecha" disabled=""/>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="pro_cod" class="control-label"># Propuesta</label>
                        <input type="text" class="form-control" id="pro_cod" disabled=""/>
                    </div>
                    <div class="form-group col-md-7">
                        <label for="cli_nombre" class="control-label">Cliente</label>
                        <input type="text" class="form-control" id="cli_nombre" disabled=""/>
                    </div>
                    <div class="form-group col-md-5">
                        <label for="cli_ci_ruc" class="control-label">CI o RUC N°</label>
                        <input type="text" class="form-control" id="cli_ci_ruc" disabled=""/>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="ord_descripcion" class="control-label">Observaciones</label>
                        <textarea id="ord_descripcion" class="form-control" placeholder="Observaciones..." maxlength="300"></textarea>
                        <div class="help-block" style="display: inline-block">Max. 300 caracteres</div>
                    </div>

                </article>
                <article class="row" id="op-detalle" style="display:none">
                    <h2>Servicios</h2>
                    <div id="ctr_ser_edit" style="display: none">
                        <div class="form-group col-md-12">
                            <label for="ser_data" class="control-label">Descripción del Servicio</label>
                            <input type="text" class="form-control" id="ser_data"readonly=""/>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="ctrl_fecha_inicio" class="control-label">Fecha de inicio</label>
                            <input type="text" class="form-control" id="ctrl_fecha_inicio"/>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="ctrl_duracion" class="control-label">Duración (Meses)</label>
                            <input oninput="orden.calcularMes()" onkeypress="orden.calcularMes()" type="text" class="form-control" id="ctrl_duracion" max="12" min="1"/>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="ctrl_fecha_fin" class="control-label">Fecha de finalización</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="ctrl_fecha_fin" disabled=""/>
                                <span class="input-group-btn fecha_fin_edit">
                                    <button class="btn btn-default" onclick="orden.editFecha()"><span class="glyphicon glyphicon-pencil"></span></button>
                                    <button class="btn btn-success" onclick="orden.saveFecha()" style="display:none"><span class="glyphicon glyphicon-ok"></span></button>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-md-3" style="text-align: right">
                            <label>&nbsp;</label>
                            <button class="btn btn-success" title="Aceptar" onclick="orden.ctrlSave()"><span class="glyphicon glyphicon-ok"></span></button>
                            <button class="btn btn-danger" title="Cancelar" onclick="orden.ctrlCancel()"><span class="glyphicon glyphicon-remove"></span></button>
                            <button class="btn btn-default" title="Valores por defecto" onclick="orden.ctrlReset()"><span class="glyphicon glyphicon-repeat"> </span></button>
                        </div>
                    </div>
                    <table class="table table-bordered table-condensed table-hover data col-md-12"  id="ctrl_servicios">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Medio</td>
                                <td>Descripcion</td>
                                <td>Monto</td>
                                <td>Fecha de Inicio</td>
                                <td>Fecha de Fin</td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <script id="t_ctrl_servicios" type="text/x-handlebars-template">
                        {{#if detalles}}
                        {{#each detalles}}
                        <tr class="b">
                        <td>{{ inc @index }}</td>
                        <td>{{ med_descripcion }}</td>
                        <td>{{ det_cantidad }}&nbsp;{{ ser_descripcion }}</td>
                        <td>{{ det_monto }}</td>
                        {{#if ctrl_fecha_inicio}}
                        <td>{{ ctrl_fecha_inicio }}</td>
                        {{else}}
                        <td>{{ toDay }}</td>
                        {{/if}}

                        {{#if ctrl_fecha_fin}}
                        <td>{{ ctrl_fecha_fin }}
                        {{else}}
                        <td>{{ mes toDay }}
                        {{/if}}

                        <div class="btn_container">
                        <button onclick="orden.m({{@index}})"><span class="glyphicon glyphicon-pencil"></span></button>
                        </div>
                        </td>
                        </tr>
                        {{/each}}
                        {{else}}
                        <tr>
                        <td colspan="6">Ningun detalle a mostrar...</td>
                        </tr>
                        {{/if}}

                    </script>
                </article>
                <article class="row" id="orden_generada" style="display: none">
                    <h2 class="col-md-12">Orden de Publicidad <span class="label label-default pull-right" id="ord_cod">#0000</span></h2>

                    <span class="col-md-6 h4"><strong>Propuesta #:&nbsp;</strong> <span id="pro_cod_"></span></span>
                    <span class="col-md-6 h4"><strong>Fecha:&nbsp;</strong><span id="pro_fecha"></span></span>
                    <span class="col-md-6 h4"><strong>Cliente:&nbsp;</strong> <span class="cli_nombre"></span></span>
                    <span class="col-md-6 h4"><strong>RUC o CI Nro.:&nbsp;</strong> <span class="cli_ci_ruc"></span></span>
                    <span class="col-md-12 h4"><strong>Titulo:&nbsp;</strong> <span id="ord_titulo_"></span></span>
                    <div class="col-md-12">
                        <p class="h4">Observaciones:</p>
                        <p id="ord_obs_">Algunas de las observaciones irán aqui.-</p>
                    </div>

                    <div class="col-md-12"> 
                        <span class="h4">Servicios</span>

                        <div class="lista-contenedor" id="detalles_orden">
                            
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <br>
                        <br>
                        <hr>
                        <p class="text-center cli_nombre"><p>
                        <p class="text-center cli_ci_ruc"><p>
                        
                    </div>
                    
                    <div class="col-md-6">
                        <br>
                        <br>
                        <hr>
                        <p class="text-center" id="emp_nombre_apellido"><p>
                        <p class="text-center" id="emp_ci"><p>                                
                    </div>
                    
                    <div class="col-md-12 text-center">
                        <br>
                        <br>
                        <br>
                        <hr>
                        <address>
                            <strong>Mega Global Comunicación</strong><br>
                            Oficina: Av. Gral. Santos 2576 esq. Agustin Yegros<br>
                            Asunción - Paraguay<br>
                            Tel.: (021) 301 219
                        </address>
                    </div>

                    <script id="t_detalles_orden" type="text/x-handlebars-template">
                        {{#if detalle}}
                            {{#each detalle}}
                                <div class="lista-item">
                                    <span class="col-md-12"><label>Medio:&nbsp;</label>{{med_descripcion}}</span>
                                    <span class="col-md-12"><label>Descripción:&nbsp; </label> {{det_cantidad}} {{ser_descripcion}}</span>
                                    <span class="col-md-6"><label>Fecha Inicio:&nbsp;</label>{{ctrl_fecha_inicio}}</span>
                                    <span class="col-md-6"><label>Fecha finalización:&nbsp;</label>{{ctrl_fecha_fin}}</span>
                                    <span class="col-md-12"><label>Costo Mensual:&nbsp;</label> Gs.{{num_fmt det_monto}}&nbsp;(<span class="text-uppercase">{{str_nums det_monto}}</span>)</span>
                                </div>
                            {{/each}}
                        {{/if}}
                    </script>
                </article>
            </div>
            <div class="botones_operativos abajo btn-group-lg text-right">
                <button class="btn btn-default" id="op-cancel" onclick="orden.cancel()" title="Cancelar"> <span class="glyphicon glyphicon-remove"></span> </button>
                <button class="btn btn-default" id="op-next" onclick="orden.next()" title="Siguiente"> <span class="glyphicon glyphicon-chevron-right"></span> </button>
                <button class="btn btn-success" id="op-save" onclick="orden.save()" title="Guardar"> <span class="glyphicon glyphicon-ok"></span> </button>
            </div>
        </div>
        <script src="app/validator/vOrdenPublicidad.js"></script>
    </body>
</html>
