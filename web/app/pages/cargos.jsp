<%-- 
    Document   : clientes
    Created on : 27-oct-2015, 19:13:20
    Author     : Damián
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%
            /*
            
             */
        %>
        <script type="text/javascript">
            /*global cargos */
        </script>
    </head>
    <body>
        <section class="main" >
            <div class="btn-group-lg botones_operativos">
                <button class="salida btn btn-danger" onclick="cargos.salir()" title="Salir"><span class="glyphicon glyphicon-remove"></span></button>
                <button id="report_ord" class="btn btn-warning" onclick="cargos.view()" title="Reporte"><span class="glyphicon glyphicon-list"></span></button>
                <button id="new" class="btn btn-success" onclick="cargos.new()" title="Nuevo"><span class="glyphicon glyphicon-plus"></span></button>
            </div>
            <section class="formulario">
                <div class="row" id="formu" style="display: none">
                    <div class="form-group col-md-12">
                        <label for="car_descri">Cargo:</label>
                        <input id="car_descri" type="text" class="form-control">
                    </div>                
                </div>
                <article class="row" id="info">
                    <h3>Gestionar Cargos</h3>
                    
                        <table class="table table-bordered table-condensed table-hover"  id="cargos">
                            <thead>
                                <tr>
                                    <td>#</td>
                                    <td>Descripcion</td>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    <script id="t_cargos" type="text/x-handlebars-template">
                       <span> 
                        <button onclick="cargos.m({{ data }})" title="Modificar {{ car_descri }}"><span class="icon-pencil"></span></button>
                        <button onclick="cargos.e({{ data }})" title="Eliminar {{ car_descri }}"><span class="icon-trashcan"></span></button>
                        </span>
                    </script>


                    <div class="info_clientes"></div>
                </article>
            </section>
        </section>
        <script src="app/validator/vCargos.js" type="text/javascript"></script>
    </body>
</html>
