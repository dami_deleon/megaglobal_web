<%-- 
    Document   : servicios
    Created on : 10/07/2015, 06:21:26 PM
    Author     : Damián
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <!-- String ser_cod = request.getParameter("ser_cod");
            String ser_descripcion = request.getParameter("ser_descripcion");
            String ser_costo_mensual = request.getParameter("ser_costo_mensual");
            String ser_costo_unitario = request.getParameter("ser_costo_unitario");
            String med_cod = request.getParameter("med_cod");-->
        <section class="main">
            <div class="arriba botones_operativos btn-group-lg">
                <button id="salida" onclick="servicios.salir()" class="btn btn-danger" title="Salir"> <span class="glyphicon glyphicon-remove"></span></button>
                <button id="report_ord" onclick="servicios.view()" class="btn btn-warning" title="Listar Servicios"><span class="glyphicon glyphicon-list"></span></button>
                <button id="new" onclick="servicios.new()" class="btn btn-success" title="Nuevo"><span class="glyphicon glyphicon-plus"></span></button>
            </div>
            <section class="formulario">
                <script id="t_title" type="text/x-handlebars-template">
                    {{ title }}
                </script>
                <article class="row" id="formu" style="display: none"> 
                    <h2 id="title"></h2>
                    <div class="form-group">
                        <label id="label_ser_descripcion">Descripción</label>
                        <input type="text" class="form-control" id="ser_descripcion" aria-describedby="help_ser_descripcion"/>
                        <div class="help-block" id="help_ser_descripcion">
                            Este campo describe el sevicio
                        </div>
                    </div>
                    <div class="form-group">
                        <label id="label_ser_costo_mensual">Costo Mensual</label>
                        
                            
                        <input type="text" class="form-control" id="ser_costo_mensual" aria-describedby="help_ser_costo_mensual"/>
                       
                        <div class="help-block" id="help_ser_costo_mensual">
                            Este campo describe el costo mensual del servicio, atendiendo que será uno (1) por programa/publicación durante el mes contratado. Indicar costo con IVA incluido.
                        </div>
                    </div>
                    <div class="form-group">
                        <label id="label_ser_costo_unitario">Costo Unitario</label>
                            <input type="text" class="form-control" id="ser_costo_unitario" aria-describedby="help_ser_costo_unitario"/>
                        <div class="help-block" id="help_ser_costo_unitario">
                            Este campo describe el costo unitario del servicio, atendiendo que será uno (1) por programa/publicación. Indicar costo con IVA incluido.
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="med_cod">Medio</label>
                        <select id="med_cod" class="form-control">
                            <option value="0">Seleccionar Medio</option>
                            <option value="1">Televisión</option>
                            <option value="2">Revista</option>
                            <option value="3">Radio</option>
                        </select>
                    </div>
                </article>
                <article class="row" id="info">
                    <h2>Servicios</h2>
                    <table id="servicios" class="table table-primary table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Medio</td>
                                <td>Descripción</td>
                                <td>Monto Unit.</td>
                                <td>Monto Mens.</td>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    <script id="t-servicios" type="text/x-handlebars-template">
                        {{#if data}}
                        {{#each data}}
                        <tr class="clickable-row b" data-index="{{@index}}">
                        <td>{{ inc @index}}</td>
                        <td>{{ med_descripcion }}</td>
                        <td>{{ ser_descripcion }}</td>
                        <td class="text-right">{{ num_fmt ser_costo_unitario }}</td>
                        <td class="text-right">{{ num_fmt ser_costo_mensual }}</td>
                        </tr>
                        {{/each}}
                        {{else}}
                        <tr>
                        <td colspan="5">Se ha producido un error al cargar los Servicios</td>
                        </tr>
                        {{/if}}
                    </script>
                </article>
            </section>
            <div class="botones_operativos text-right btn-group-lg abajo">
                <button  id="save" onclick="servicios.save()" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span></button>
                <button  id="save_new" onclick="servicios.save_new()" class="btn btn-warning" title="Guardar Cambios"><span class="glyphicon glyphicon-ok"></span></button>
                <button onclick="servicios.cancel()" id="cancel" class="btn btn-default" title="Cancelar"><span class="glyphicon glyphicon-remove"></span></button>
            </div>
        </section>
        <script src="app/validator/vServicios.js" type="text/javascript"></script>

    </body>
</html>
