<%-- 
    Document   : cobros
    Created on : 27-jul-2016, 23:41:38
    Author     : Damián
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>cobros</title>
    </head>
    <body>
        <section class="main">
            <div class="btn-group-lg botones_operativos">
                <button id="cobro_nuevo" class="btn btn-success" onclick="cobros.new()"><span class="glyphicon glyphicon-plus"></span></button>
                <button id="reportar_cobros" class="btn btn-warning" onclick="cobros.view()"><span class="glyphicon glyphicon-list"></span></button>
                <button id="salir_cobros" class="btn btn-danger" onclick="cobros.salir()"><span class="glyphicon glyphicon-remove"></span></button>
            </div>
            <section class="formulario">
                <article class="row" id="cob-facturas_pendientes">
                    <h2>Facturas Pendientes de Cobro</h2>
                    <table class="table table-bordered table-primary" id="facturas_pendientes">
                        <thead>
                            <tr>
                                <td>Orden #</td>
                                <td>Razón Social</td>
                                <td>RUC o CI N&ordm;</td>
                                <td>Fecha de Emisión</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="4">Cargando... <img src="app/images/rolling.svg"/> </td>
                            </tr>
                        </tbody>

                    </table>


                    <script id="t_facturas_pendientes" type="text/x-handlebars-template">

                        {{#if facturas_pendientes}} 
                            {{#each facturas_pendientes}}
                                <tr class="clickable-row b" data-index="{{@index}}">
                                    <td>{{ ord_cod }}</td>
                                    <td>{{ ven_metadata.razon_social }}</td>
                                    <td>{{ ven_metadata.ruc }}</td>
                                    <td>{{ ven_fecha }}</td>
                                </tr>
                            {{/each}} 
                        {{else}}
                                <tr>
                                    <td colspan="4">Ninguna factura pendiente de Cobro</td>
                                </tr>
                        {{/if}}
                    </script>

                </article>
                <article class="row" id="cob-cobranza" style="display: none">
                    <h3>Cobranza</h3>
                    <div class="form-group col-md-7">
                        <label for="cob-cli_nombre" class="control-label">Nombre o Razón Social</label>
                        <div class="input-group">
                            <input type="text" class="form-control" disabled="" id="cob-cli_nombre"/>
                            <span class="input-group-btn">
                                <button class="btn btn-default btn-cob-cli_nombre" onclick="cobros.editCampo('cob-cli_nombre')"><span class="glyphicon glyphicon-pencil"></span></button>
                                <button class="btn btn-success btn-cob-cli_nombre" onclick="cobros.saveCampo('cob-cli_nombre')" style="display:none"><span class="glyphicon glyphicon-ok"></span></button>
                            </span>
                        </div>
                    </div>

                    <div class="form-group col-md-5">
                        <label for="cob-cli_ci_ruc" class="control-label">CI / RUC</label>
                        <div class="input-group">
                            <input type="text" class="form-control" disabled="" id="cob-cli_ci_ruc"/>
                            <span class="input-group-btn">
                                <button class="btn btn-default btn-cob-cli_ci_ruc" onclick="cobros.editCampo('cob-cli_ci_ruc')"><span class="glyphicon glyphicon-pencil"></span></button>
                                <button class="btn btn-success btn-cob-cli_ci_ruc" onclick="cobros.saveCampo('cob-cli_ci_ruc')" style="display:none"><span class="glyphicon glyphicon-ok"></span></button>
                            </span>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <h3>Comprobantes</h3>
                        <table id="cob-comprobantes" class="table table-condensed table-bordered table-primary">
                            <thead>
                                <tr>
                                    <td title="Tipo de Comprobante">T.C.</td>
                                    <td>Fecha</td>
                                    <td title="Número">Nro.</td>
                                    <td>Importe</td>
                                </tr>
                            </thead>
                            <tbody>


                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>
                        <script id="t-cob-comprobantes" type="text/x-handlebars-template">
                            {{#if comprobantes}} 
                            {{#each comprobantes}}
                            <tr class="clickable-row b" data-index="{{@index}}">
                            <td>{{con_descripcion}}</td>
                            <td>{{ven_fecha}}</td>
                            <td>{{ ven_nro_factura}}</td>
                            <td class="text-right">{{num_fmt cta_monto_total }}</td>
                            </tr>
                            {{/each}} 
                            {{else}}
                            <tr>
                            <td colspan="3">Ningún comprobante añadido</td>
                            </tr>
                            {{/if}}
                        </script>

                        <script id="t-cob-comprobantes-total" type="text/x-handlebars-template">
                            <tr class="active">
                            <td colspan="3">Total</td>
                            <td class="text-right ">{{num_fmt cta_monto_total }}</td>
                            </tr>
                        </script>
                        <button class="btn btn-success col-md-12" onclick="cobros.nuevoAddComprobante()" type="button" title="Agregar Medio de pago">Agregar Comprobante</button>

                    </div>
                    <div class="form-group col-md-6">
                        <h3>Medios de Pago</h3>
                        <table class="table table-condensed table-bordered table-primary" id="m_pago">
                            <thead>
                                <tr>
                                    <td>Medio de Pago</td>
                                    <td>Monto</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="danger">
                                    <td colspan="2">No se ha indicado ningún medio de pago <span class="glyphicon glyphicon-remove"></span></td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr id="row-total_pagar_num">
                                    <td>Total Guaraníes</td>
                                    <td class="text-right" id="cob-total_pagar_num">0</td>
                                </tr>
                            </tfoot>
                        </table>
                        <div id="help-totalPago" class="help-block" style="display: block">
                            
                        </div>
                        <script id="t_mediopago" type="text/x-handlebars-template">
                            {{#if medios_pago}} 
                                {{#each medios_pago}}
                                    <tr class="clickable-row b" data-index="{{@index}}">
                                        <td>{{ medio }}</td>
                                        <td class="text-right">{{ num_fmt monto }}</td>
                                    </tr>
                                {{/each}}
                            {{else}}
                                <tr class="danger">
                                    <td colspan="2">No se ha indicado ningún medio de pago <span class="glyphicon glyphicon-remove"></span></td>
                                </tr>
                            {{/if}}
                        </script>
                        <div class="btn-block">
                            <button class="btn btn-success col-md-12" onclick="cobros.nuevoMedioPago()" type="button" title="Agregar Medio de pago">Agregar Medio de Pago</button>
                        </div>
                    </div>
                    <div class="modal fade" id="dialog_mediopago">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title">Agregar Medio de Pago</h4>

                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <select id="med_cod" class="selectpicker form-control">
                                            <option value="0">Seleccionar Medio de Pago</option>
                                            <option value="1">Efectivo</option>
                                            <option value="2">Cheque</option>
                                            <option value="3">Tarjeta</option>
                                            <option value="4">Transferencia Bancaria</option>
                                        </select>
                                    </div>
                                    <div class="medio" id="efectivo">
                                        <div class="form-group">
                                            <label for="efe_monto">Monto</label>

                                                <input id="efe_monto" type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="medio" id="cheque">

                                        <div class="form-group">    
                                            <label for="cheq_numero">Numero de Cheque</label>
                                            <input id="cheq_numero" type="text" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="cheq_fecha_emision">Fecha emision del cheque</label>
                                            <input id="cheq_fecha_emision" type="text" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="ent_cod">Entidad Bancaria</label>
                                            <select id="ent_cod" class="selectpicker form-control" data-live-search="true">
                                                <option value="0">Cargando...</option>
                                            </select>
                                        </div>
                                        <script id="t-ent_cod" type="text/x-handlebars-template">
                                            {{#if entidades}} 
                                                    <option value="0">Seleccionar Entidad</option>
                                                {{#each entidades}}
                                                    <option value="{{ent_cod}}">{{ent_descripcion}}</option>
                                                {{/each}} 
                                            {{else}}
                                                <option value="0">No se han podido encontrar...</option>
                                            {{/if}}
                                        </script>
                                        <div class="form-group">
                                            <label for="cheq_tipo_cod">Tipo Cheque</label>
                                            <select id="cheq_tipo_cod" class="selectpicker form-control" data-live-search="true">
                                                <option value="0">Seleccionar tipo de cheque</option>
                                                <option value="1">Normal</option>
                                                <option value="2">Financiera</option>
                                                <option value="3">Diferido</option>
                                                <option value="4">Al portador</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="cheq_monto">Monto</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">Gs.</span>
                                                <input id="cheq_monto" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-success" id="addMedioPago" onclick="cobros.addMedioPago()">Aceptar</button>
                                    <button type="button" class="btn btn-warning" id="saveMedioPago" style="display: none">Guardar Cambios</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
                <article class="row" id="cb-clientes" style="display: none">
                    <h3>Clientes</h3>
                    <div class="form-group">
                        <select id="cb-cli_cod" class="form-control">
                            <option value="0">Cargando...</option>
                        </select>
                        <div class="help-block" style="display: block">
                            Busca y selecciona el cliente para cargar sus cuentas pendientes.
                        </div>
                    </div>


                    <script id="cb-t_cli_cod" type="text/x-handlebars-template">
                        {{#if clientes}}
                            <option value="0">Seleccionar Cliente</option>
                        {{#each clientes}}
                            <option value="{{cli_cod}}" class="h4">{{ cli_nombre }}&nbsp; <span class="small">{{ cli_ci_ruc }}</span></option>
                        {{/each}}
                        {{else}}
                            <option value="0">No hay clientes para mostrar<option>
                        {{/if}}
                    </script>

                    <table class="table table-bordered table-hover table-responsive table-striped" id="cb-cuentas_cliente" style="display: none">
                        <thead>
                            <tr>
                                <td><span class="glyphicon glyphicon-ok-sign"></span></td>
                                <td title="Código de la Cuenta">Cod. Cuenta</td>
                                <td>Vencimiento</td>
                                <td>Saldo Cuenta</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="4">Cargando... <img src="app/images/rolling.svg"></td>
                            </tr>
                        </tbody>
                    </table>
                    <script id="cb-t_cuentas_cliente" type="text/x-handlebars-template">
                        {{#if cuentas}}
                        {{#each cuentas}}
                        <tr class="b">
                        <td class="text-center"> <input type="radio" value="{{@index}}" class="ck" name="cuenta_cobrar"></td>
                        <td>{{cta_cod}}</td> 
                        <td>{{ cta_fecha_vencimiento }}</td>
                        <td>{{ cta_monto_cobrar }}

                        </td>
                        </tr>
                        {{/each}}
                        {{else}}
                        <tr>
                        <td colspan="4">Ninguna cuenta pendiente el dia de hoy</td>
                        </tr>
                        {{/if}}
                    </script>
                </article>

                <article class="row" id="formulario" style="display: none">
                    <h2>Gestionar Cobro</h2>
                    <div class="form-group col-md-6">
                        <label for="cob_cod">Cobro N&ordm;</label>
                        <input id="cob_cod" type="text" class="form-control" readonly="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="cob_fecha">Fecha</label>
                        <input id="cob_fecha" type="text" class="form-control" readonly="">
                    </div>
                </article>
                <article class="row" id="formulario" style="display: none">
                    <h3>Detalles de la cuenta</h3>
                    <table class="tb_ table col-md-12" id="facturas">
                        <tr>
                            <td>Factura Nro</td>
                            <td>Fecha Factura</td>
                            <td>Monto Gs.</td>
                            <td colspan="2">Cond. Venta</td>
                        </tr>
                        <tr>
                            <td colspan="4">Cargando...</td>
                            <td></td>
                        </tr>
                    </table>
                    <!--- Plantilla de facturas -->
                    <script id="t_facturas" type="text/x-handlebars-template">
                        <tr>
                        <td>Factura Nro</td>
                        <td>Fecha Factura</td>
                        <td>Monto Gs.</td>
                        <td colspan="2">Cond. Venta</td>
                        </tr>
                        {{#if facturas}} {{#each facturas}}
                        <tr>
                        <td>{{ fac_nro }}</td>
                        <td>{{ ven_fecha }}</td>
                        <td>{{ ven_monto }}</td>
                        <td>{{ ven_cond }}</td>
                        <td>
                        <button type="button" onclick="cobros.ver_factura({{@index}})"><span class="icon-pencil"></span></button>
                        <button type="button" id="cobrar"><span class="icon-info"></span></button>
                        </td>
                        </tr>
                        {{/each}} {{else}}
                        <tr>
                        <td colspan="4">No se han cargado los datos</td>
                        <td></td>
                        </tr>
                        {{/if}}
                    </script>
                    <div class="form-group col-md-12">
                        <label for="cob_fecha">&nbsp;</label>
                        <button type="button" class="form-control btn btn-success" title="Agregar Factura" onclick="openDialog('plantilla_factura', 'Agregar Factura')">Agregar Factura</button>
                    </div>
                </article>

                <article class="row" id="formulario" style="display: none">
                    <h3>Detalle del Cobro</h3>
                    <div class="form-group col-md-5">
                        <label for="cob_fecha">Monto a Cobrar</label>
                        <div class="input-group">
                            <input id="" type="text" class="form-control">
                            <span class="input-group-addon">Gs.</span>
                        </div>
                    </div>
                    <div class="form-group col-md-7">
                        <label for="cob_fecha">Monto Cobrado</label>
                        <div class="input-group">
                            <input id="" type="text" class="form-control">
                            <span class="input-group-addon">Gs.</span>
                        </div>
                    </div>

                </article>
            </section>
            <div class="btn-group-lg text-right botones_operativos abajo">
                <button class="btn btn-default" title="Atrás"><span class="glyphicon glyphicon-chevron-left"></span></button>
                <button class="btn btn-success" id="co-save" onclick="cobros.saveCobro()"title="Guardar"><span class="glyphicon glyphicon-ok"></span></button>
                <button class="btn btn-default" id="co-cancel" title="Cancelar"><span class="glyphicon glyphicon-remove"></span></button>
                <button class="btn btn-default" title="Siguiente"><span class="glyphicon glyphicon-chevron-right"></span></button>
            </div>
        </section>
        <script type="text/javascript" src="app/validator/vCobros.js"></script>

    </body>
</html>
