/* 
 * Copyright 2016 damian.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* global alertify, context */

var cobros = {};
cobros.url = '/megaglobal_web_2/cobros';
cobros.data_facturas_pendientes = [];
cobros.medios_pago = [];

cobros.new = function () {

}

cobros.view = function () {

};

cobros.salir = function () {
    home();
};

cobros.facturas_pendientes = function () {
    $.get(cobros.url, {o: "facturas_pendientes"}, function (resp) {
        if (resp.error)
            alertify.error(resp.detail);
        for (var i = 0; i < resp.facturas_pendientes.length; i++) {
            var d = resp.facturas_pendientes[i].ven_metadata;
            var json = JSON.parse(d.replace(/(\s\s)+/g, ""));
            resp.facturas_pendientes[i].ven_metadata = json;
        }
        cobros.data_facturas_pendientes = resp.facturas_pendientes;
        renderizarHTML(resp, 't_facturas_pendientes', 'facturas_pendientes tbody');
    });
};

cobros.ver_factura = function (index) {
    var data = cobros.data_facturas_pendientes[index];
    var url = "/megaglobal_web_2/#/factura?ven_nro_factura="+ data.ven_nro_factura +"&tal_cod="+data.tal_cod;
    window.open(url, '_blank');
};

cobros.saveCobro = function (i) {
    
    console.log(cobros.medios_pago);
};

cobros.actualizarPagos = function () {
    var suma = 0;
    for (var i = 0; i < cobros.medios_pago.length; i++) {
        suma += Number(cobros.medios_pago[i].monto);
    }

    cobros.sumaPago = suma;
    $("#cob-total_pagar_num").html(formato_nro(suma));
};

cobros.entidades = function () {
    $.get(cobros.url, {o: "entidades"}, function (resp) {
        if (resp.error)
            alertify.error(resp.detail);
        renderizarHTML(resp, 't-ent_cod', 'ent_cod');
        renderizarHTML(resp, 't-ent_cod', 'tar_ent_cod');
        renderizarHTML(resp, 't-ent_cod', 'dep_ent_cod');
        $('.selectpicker').selectpicker('refresh');
    });
};

cobros.addMedioPago = function () {
    
    switch ($('#med_cod').val()) {
        //si es efectivo
        case '1':
            if (validar_campo(['efe_monto'])) {
                var medioPago = {
                    medio: 'Efectivo',
                    med_cod: 1,
                    monto: $('#efe_monto').val()
                };
                cobros.comprobarMedioPago(medioPago, 'med_cod');
            }
            break;
        case '2':

            if (validar_campo(['cheq_fecha_emision', 'ent_cod', 'cheq_tipo_cod', 'cheq_monto', 'cheq_numero'])) {
                var medioPago = {
                    medio: 'Cheque',
                    med_cod: 2,
                    monto: $('#cheq_monto').val(),
                    cheq_monto: $('#cheq_monto').val(),
                    ent_cod: $('#ent_cod').val(),
                    cheq_tipo_cod: $('#cheq_tipo_cod').val(),
                    cheq_fecha_emision: $('#cheq_fecha_emision').val(),
                    cheq_numero: $('#cheq_numero').val()
                };
                cobros.comprobarMedioPago(medioPago, 'cheq_numero');
            };
            break;
        case '3':
            if(validar_campo(['tarj_tipo_cod', 'tar_ent_cod', 'tar_nro_transaccion', 'tar_monto'])){
                var medioPago = {
                    medio: 'Tarjeta',
                    med_cod: 3,
                    monto: $('#tar_monto').val(),
                    tar_monto: $('#tar_monto').val(),
                    ent_cod: $('#tar_ent_cod').val(),
                    tar_nro_transaccion: $('#tar_nro_transaccion').val(),
                    tarj_tipo_cod: $('#tarj_tipo_cod').val()
                }
            }
            break;
        case '4':

            break;
    }
};

//genera ventana de pago para editar
cobros.escribirPago = function (data, i) {
    $('#addMedioPago').hide();
    $('#saveMedioPago').show();
    
    switch (data.med_cod) {
        case 1:
            $('#efe_monto').val(data.monto);
            $('#saveMedioPago').on('click', function(){
                cobros.saveMedioPago(i);
            });
            break;
        case 2:
            $('#cheq_monto').val(data.cheq_monto);
            $('#ent_cod').selectpicker('val', data.ent_cod);
            $('#cheq_tipo_cod').selectpicker('val', data.cheq_tipo_cod);
            $('#cheq_fecha_emision').val(data.cheq_fecha_emision);
            $('#cheq_numero').val(data.cheq_numero);
            
            $('#saveMedioPago').on('click', function(){
                cobros.saveMedioPago(i);
            });
            break;
        case 3:

            break;
        case 4:

            break;
    }
};

cobros.saveMedioPago = function(i){
    
    switch ($('#med_cod').val()) {
        //si es efectivo
        case '1':
            if (validar_campo(['efe_monto'])) {
                var medioPago = {
                    medio: 'Efectivo',
                    med_cod: 1,
                    monto: $('#efe_monto').val()
                };
                cobros.medios_pago[context.dataindex] = medioPago;
                $('#dialog_mediopago').modal('hide');
                cobros.resetDialogMedioPago();
                renderizarHTML(cobros, 't_mediopago', 'm_pago tbody');
                cobros.actualizarPagos();
                $('#dialog_mediopago').resetStyles();
                cobros.comprobarImporte();
            }
            break;
        case '2':

            if (validar_campo(['cheq_fecha_emision', 'ent_cod', 'cheq_tipo_cod', 'cheq_monto', 'cheq_numero'])) {
                var medioPago = {
                    medio: 'Cheque',
                    med_cod: 2,
                    monto: $('#cheq_monto').val(),
                    cheq_monto: $('#cheq_monto').val(),
                    ent_cod: $('#ent_cod').val(),
                    cheq_tipo_cod: $('#cheq_tipo_cod').val(),
                    cheq_fecha_emision: $('#cheq_fecha_emision').val(),
                    cheq_numero: $('#cheq_numero').val()
                };
                cobros.medios_pago[context.dataindex] = medioPago;
                $('#dialog_mediopago').modal('hide');
                cobros.resetDialogMedioPago();
                renderizarHTML(cobros, 't_mediopago', 'm_pago tbody');
                cobros.actualizarPagos();
                resetStyles();
                cobros.comprobarImporte();
            }
            ;
            break;
        case '3':

            break;
        case '4':

            break;
    }
};

cobros.comprobarMedioPago = function (medioPagoObj, campo) {
    var f = [];
    var obj = new Object();
   
    
    function filtrMedio(x) {
        return x[campo] === this[campo];
    };
    
    if (cobros.medios_pago.length > 0)
        f = cobros.medios_pago.filter(filtrMedio, medioPagoObj);
    
    if (f.length > 0) {
        alertify.error("Ya se agregó <b>" + medioPagoObj.medio + "</b> a la lista");
    } else {
        cobros.medios_pago.push(medioPagoObj);
        $('#dialog_mediopago').modal('hide');
        cobros.resetDialogMedioPago();
        renderizarHTML(cobros, 't_mediopago', 'm_pago tbody');
        cobros.actualizarPagos();
        resetStyles();
        cobros.comprobarImporte();
    }
    ;
};


cobros.verificarMedioPago = function(clave){
    
};

cobros.comprobarImporte = function () {

    $('#row-total_pagar_num').removeClass('danger').removeClass('warning');
    $('#help-totalPago').html('');
    if (cobros.facturaTotal > cobros.sumaPago) {
        $('#row-total_pagar_num').addClass('danger');
        $('#help-totalPago').html('Se ha ingresado menos del total');
    }

    if (cobros.facturaTotal < cobros.sumaPago) {
        $('#row-total_pagar_num').addClass('warning');
        $('#help-totalPago').html('Se ha ingresado más del total');
    }
    ;
};

cobros.resetDialogMedioPago = function () {
    $('#dialog_mediopago').find('input').val('');
    $('#dialog_mediopago').find('select').selectpicker('val','0');
    $('#med_cod').selectpicker('val',0);
    //habilitar los camppos de los medios de pago
    cobros.medioPagoCampos();
};

cobros.nuevoMedioPago = function () {
    cobros.resetDialogMedioPago();
    
    
    var t = Number(cobros.facturaTotal);
    var p = cobros.sumaPago || 0;
    if (t <= p)
        return alertify.error("<b>Error!</b> Ya se ha alcanzado el monto total");
    
    $('#addMedioPago').show();
    $('#saveMedioPago').hide();
    $('#dialog_mediopago').modal('show');
};

cobros.editMedioPago = function (i) {
    var m = cobros.medios_pago[i];
    $('#med_cod').prop('disabled', false);
    // abrir ventana
    $('#dialog_mediopago').modal('show');
    //seleccionar medio pago
    $('#med_cod').val(m.med_cod);
    //habilitar los camppos de los medios de pago
    cobros.medioPagoCampos();
    //bloquear select de medio de pago
    $('#med_cod').prop('disabled', true);
    $('#med_cod').selectpicker('refresh');
    //console.log(m);
    cobros.escribirPago(m, i);
};

cobros.elimMedioPago = function (i) {
    
    alertify.confirm("¿Estás seguro de <b>eliminar</b> este medio de pago?<br>\n\
                    <dl><dt>Medio</dt><dd>"+ cobros.medios_pago[i].medio +"</dd>\n\
                    <dt>Monto</dt><dd>"+ cobros.medios_pago[i].monto +"</dd></dl> ", function (e) {
        if (e) {
            cobros.medios_pago.splice(i, 1);
            renderizarHTML(cobros, 't_mediopago', 'm_pago tbody');
            cobros.actualizarPagos();
            resetStyles();
            cobros.comprobarImporte();
        }
    });
    
};
cobros.cobrarFactura = function (i) {
    $("#cob-facturas_pendientes").hide(efecto());
    $('#cob-cobranza').show(efecto());
    $('#co-save').show();
    $('#co-cancel').show();
    //console.log(JSON.stringify(cobros.data_facturas_pendientes[i]));
//    window.console.log(i);
    var factura = cobros.data_facturas_pendientes[Number(i)];
    factura.comprobantes = [{
            con_descripcion: factura.con_descripcion,
            ven_fecha: factura.ven_fecha,
            ven_nro_factura: factura.ven_nro_factura,
            cta_monto_total: factura.cta_monto_total
        }];
    cobros.facturaTotal = factura.cta_monto_total;
    $('#cob-cli_nombre').html(factura.ven_metadata.razon_social);
    $('#cob-cli_ci_ruc').html(factura.ven_metadata.ruc);
    renderizarHTML(factura, 't-cob-comprobantes', 'cob-comprobantes tbody');
    renderizarHTML(factura, 't-cob-comprobantes-total', 'cob-comprobantes tfoot');
    
};
cobros.cuentasPendientesMenu = function () {
    //tabla facturas pendientes
    context.attach('#facturas_pendientes tbody tr', [
        {
            header: 'Opciones'
        },
        {
            icon: 'glyphicon-info-sign',
            text: 'Ver factura',
            action: function () {
                $('.clickable-row').removeClass('active');
                cobros.ver_factura(context.dataindex);
            }
        },
        {
            icon: 'glyphicon-credit-card',
            text: 'Cobrar',
            action: function () {
                $('.clickable-row').removeClass('active');
                cobros.cobrarFactura(context.dataindex);
            }
        }
    ]);
};
cobros.mediosDePagoMenu = function () {
    context.attach('#m_pago tbody .clickable-row', [
        {
            header: 'Medios de pago'
        },
        {
            icon: 'glyphicon-pencil',
            text: 'Editar',
            action: function () {
                $('.clickable-row').removeClass('active');
                cobros.editMedioPago(context.dataindex);
            }
        },
        {
            icon: 'glyphicon-remove',
            text: 'Eliminar',
            action: function () {
                $('.clickable-row').removeClass('active');
                cobros.elimMedioPago(context.dataindex);
            }
        }
    ]);
};
cobros.medioPagoCampos = function () {
    var t = Number(cobros.facturaTotal);
    var p = cobros.sumaPago || 0;
    switch ($('#med_cod').val()) {
        case '0':
            $('.medio').hide();
            break;
        case '1':
            $('.medio').hide();
            $('#efectivo').show();
            $('#efe_monto').val(t - p);
            $('#efe_monto').focus();
            break;
        case '2':
            $('.medio').hide();
            $('#cheque').show();
            $('#cheq_monto').val(t - p);
            break;
        case '3':
            $('.medio').hide();
            $('#tarjeta').show();
            $('#tar_monto').val(t - p);
            break;
        case '4':
            $('.medio').hide();
            $('#bank_transfer').show();
            $('#dep_monto').val(t - p);
            break;
    }
};
cobros.editCampo = function (campo) {
    $('#' + campo).prop("disabled", false);
    $('#' + campo).focus();
    $('.btn-' + campo).toggle();
};
cobros.saveCampo = function (campo) {
    if (!validar_campo([campo]))
        return false;
    $('#' + campo).parent('.form-group').removeClass('has-error');
    $('#' + campo).prop("disabled", true);
    $('.btn-' + campo).toggle();
    resetStyles();
};
$(document).ready(function () {
    document.title = 'Cobros | Mega Global';
    //comprobaar apertura y cierre de caja
    cobros.facturas_pendientes();
    cobros.entidades();
    $('#cheq_fecha_emision').datepicker();
    $('#facturas_pendientes').buscador();
    $('#facturas_pendientes').activarFila();
    $('#cob-comprobantes').activarFila();
    $('#m_pago').activarFila();
    $('#med_cod').on('change', function (evt, item) {
        cobros.medioPagoCampos();
    });
    /**
     * context menu
     */
    context.init({
        fadeSpeed: 100,
        above: 'auto',
        preventDoubleContext: true,
        compress: false
    });
    cobros.cuentasPendientesMenu();
    cobros.mediosDePagoMenu();
    $('.selectpicker').selectpicker('refresh');
    
    $("#efe_monto").TouchSpin({
        verticalbuttons: true,
        max: 1000000000,
        stepinterval: 1,
        maxboostedstep: 10000000,
        prefix: 'Gs.'
    });
});