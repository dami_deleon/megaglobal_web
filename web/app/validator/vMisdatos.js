
/* global alertify */

var misdatos = {};
misdatos.url = '/megaglobal_web_2/misdatos';


misdatos.salir = function () {
    home();
};


misdatos.cancel = function () {
    resetStyles();
    var data = misdatos.data;

    $('#emp_ci').val(data.emp_ci);
    $('#emp_apellido').val(data.emp_apellido);
    $('#emp_nombre').val(data.emp_nombre);
    $('#emp_direccion').val(data.emp_direccion);
    $('#emp_fecha_nacimiento').val(data.emp_fecha_nacimiento);
    $('#ciu_cod').selectpicker('refresh');
    $('#ciu_cod').selectpicker('val', data.ciu_cod);

    $("#md_save").hide();
    $("#md_cancel").hide();
};

misdatos.cargarDatos = function () {
    $.get(misdatos.url, 'o=r', function (data) {
        $('#emp_ci').val(data.emp_ci);
        $('#emp_apellido').val(data.emp_apellido);
        $('#emp_nombre').val(data.emp_nombre);
        $('#emp_direccion').val(data.emp_direccion);
        $('#emp_fecha_nacimiento').val(data.emp_fecha_nacimiento);
        $('#ciu_cod').selectpicker('refresh');
        $('#ciu_cod').selectpicker('val', data.ciu_cod);
        misdatos.pk = data.emp_cod;
        misdatos.data = data;
        $('.form-control').on("change", function () {
            $("#md_save").show();
            $("#md_cancel").show();
        });
    });
};

misdatos.guardarDatos = function () {

    if (!validar_campo(['emp_ci', 'emp_apellido', 'emp_nombre', 'emp_direccion', 'emp_fecha_nacimiento', 'ciu_cod']))
        return alertify.error("Todos los campos son requeridos");


    var data = {
        "o": "m",
        "emp_ci": $('#emp_ci').val(),
        "emp_apellido": $('#emp_apellido').val(),
        "emp_nombre": $('#emp_nombre').val(),
        "emp_direccion": $("#emp_direccion").val(),
        "emp_fecha_nacimiento": $('#emp_fecha_nacimiento').val(),
        "ciu_cod": $('#ciu_cod').val(),
        "emp_cod": misdatos.pk
    };



    alertify.confirm("¿Estás seguro de guardar?", function (e) {
        if (e) {
            $.get(misdatos.url, data, function (data) {
                if (data !== '0') {
                    misdatos.cargarDatos();
                    alertify.success('Se ha guardado con éxito');
                    resetStyles();
                    $("#md_save").hide();
                    $("#md_cancel").hide();
                } else {
                    alertify.error('Ha ocurrido un error');
                }
            });
        }
    });
};

misdatos.ciudades = function () {
    $.get('/megaglobal_web_2/ciudad', 'o=r', function (data) {

    });

    $.ajax({
        url: '/megaglobal_web_2/ciudad',
        data: {o: 'r'},
        success: function (data, textStatus, jqXHR) {
            renderizarHTML(data, 't_ciudades', 'ciu_cod');
            $('.selectpicker').selectpicker('refresh');
            var cod = misdatos.data.ciu_cod || 0;
            $('#ciu_cod').selectpicker('val', cod);
        }
    });
};




$(document).ready(function () {

    if (urlParam('o') === "") {
        document.title = 'Mis datos | Mega Global';
        $('#ciu_cod').selectpicker();
        misdatos.ciudades();
        misdatos.cargarDatos();

        $('#emp_fecha_nacimiento').datepicker();
        $('#misdatos_form').show(efecto());
    }else{
        document.title = 'Establecer contraseña | Mega Global';
        misdatos.establecer_contrasena(urlParam('token'));
    }


});