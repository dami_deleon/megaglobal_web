/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* global esp, Handlebars, alertify, context, domtoimage */

var propuestas = {};
propuestas.detalle = [];
propuestas.url = '/megaglobal_web_2/propuestas';
propuestas.propGenerada = {};

propuestas.listarPropuestas = function () {
    $.get(propuestas.url, "o=r", function (response) {
        renderizarHTML(response, 't_propuestas', 'propuestas tbody');
    }).done(function () {
        alertify.success("Listado de Propuestas actualizadas <span class='glyphicon glyphicon-ok'></span>");
    });
};

propuestas.verPropuesta = function (p) {
    alertify.message("Se mostrará la <b>Propuesta #" + p + "</b>");
    $.get(propuestas.url, "o=p&pro_cod=" + p, function (response) {
        //datos de cliente y empleado
        $("#cli_nombre").html(response.cli_nombre);
        $("#pro_fecha_").html(response.pro_fecha);
        $("#pro_cod").html(padLeft(p, 4));
        $("#emp_nombre_apellido").html(response.emp_nombre + " " + response.emp_apellido);

        //datos de los servicios
        propuestas.propGenerada.detalle = [];
        var deta;
        var s = 0;
        for (var k = 0; k < response.detalle.length; k++) {

            if (k === 0) { // si es primero
                deta = response.detalle[k].med_cod;
                propuestas.propGenerada.detalle.push({
                    "medio": response.detalle[k].med_descripcion,
                    "servicio": [
                        {
                            "det_cantidad": response.detalle[k].det_cantidad,
                            "ser_descri": response.detalle[k].ser_descripcion,
                            "det_monto": Number(response.detalle[k].det_monto)
                        }
                    ],
                    "totalMedio": Number(response.detalle[k].det_monto)
                }
                );

            } else if (deta === response.detalle[k].med_cod) { // si no es primero y los medios son iguales
                propuestas.propGenerada.detalle[s].servicio.push(
                        {
                            "det_cantidad": response.detalle[k].det_cantidad,
                            "ser_descri": response.detalle[k].ser_descripcion,
                            "det_monto": Number(response.detalle[k].det_monto)
                        });
                propuestas.propGenerada.detalle[s].totalMedio = (
                        propuestas.propGenerada.detalle[s].totalMedio +
                        Number(response.detalle[k].det_monto));
            } else { //si es diferente
                deta = response.detalle[k].med_cod;
                propuestas.propGenerada.detalle.push({
                    "medio": response.detalle[k].med_descripcion,
                    "servicio": [
                        {
                            "det_cantidad": response.detalle[k].det_cantidad,
                            "ser_descri": response.detalle[k].ser_descripcion,
                            "det_monto": Number(response.detalle[k].det_monto)
                        }
                    ],
                    "totalMedio": Number(response.detalle[k].det_monto)}
                );
                s++;//nuevo indice del array
            }
            ;
        }
        ;
        propuestas.sumaTotalPropuesta(propuestas.propGenerada.detalle);
        renderizarHTML(propuestas.propGenerada, "t_participacion", "participacion tbody");



    }).done(function () {
        alertify.success("Se ha recuperado setisfactoriamente la Propuesta #" + p);
        propuestas.deshabilitaFormulario();
        $('#vistaPropuestas').hide(efecto());
        $('#propuesta_generada').show(efecto);
        $('#new').hide();//oculta btn nuevo
        $('#cancel').show();//muestra btn cancelar
        $('#print').show();
    });
};


propuestas.sumaTotalPropuesta = function (obj) {
    var suma = 0;
    for (var i = 0; i < obj.length; i++) {
        suma = obj[i].totalMedio + suma;
    }
    ;
    $("#total_propuesta").html(formato_nro(suma));
    $("#monto_str").html(num_str(suma));
};


propuestas.cargarClientes = function () {
    $.get('/megaglobal_web_2/propuestas?o=clientes', function (response) {
        renderizarHTML(response, 't-cli_cod', 'cli_cod');
        $('#cli_cod').selectpicker();
    });
};

propuestas.limpiarFormDetalle = function () {
    $('#ser_descripcion_data').val('');
    $('#ser_costoestandar_data').val('');
    $("#det_cantidad").val("");
    $('#ser_costo_data').val("");
    $('#ser_cod').focus();
};


propuestas.addServicio = function () {

    var f = [];
    if (!validar_campo(['det_cantidad', 'ser_cod', 'ser_descripcion_data', 'ser_costo_data'])) {
        return false;
    } else {
        resetStyles();
    }


    var d = {
        "det_cantidad": Number($("#det_cantidad").val()), //cantidad
        "ser_cod": Number($("#ser_cod").val()), // codigo del servicio
        "monto_standar": $('#ser_costoestandar_data').val(), //codto estandar
        "det_monto": $('#ser_costo_data').val(), //monto de venta
        "ser_descripcion_data": $('#ser_descripcion_data').val() // descripcion
    };

    function filtraServicios(obj) {
        if (obj.ser_cod == this) {
            return true;
        } else {
            return false;
        }
    }

    if (propuestas.detalle.length > 0)
        f = propuestas.detalle.filter(filtraServicios, d.ser_cod);

    if (f.length > 0) {
        alertify.error("Ya se agregó " + d.ser_descripcion_data + " a la lista");
    } else {
        $("#ser_cod").val("");
        $("#ser_cod").focus();
        propuestas.detalle.push(d);
        renderizarHTML(propuestas, 't_detalles', 'servicios_detalles tbody');
        propuestas.actualizarTotal();
        propuestas.limpiarFormDetalle();

    }
};


propuestas.actualizarTotal = function () {
    var suma = 0;
    for (var i = 0; i < propuestas.detalle.length; i++) {
        suma += Number(propuestas.detalle[i].det_monto);
    }

    propuestas.detalleTotal = suma;
    renderizarHTML(propuestas, 't_detalles_total', 'servicios_detalles tfoot');
};


propuestas.editPrecio = function () {
    $('#ser_costo_data').prop("readonly", false);
    $('#ser_costo_data').focus();
    $('.precio_edit').find('button').toggle();
};


propuestas.savePrecio = function () {
    if (!validar_campo(['ser_costo_data']))
        return false;
    if (!/^([0-9])*$/.test($("#ser_costo_data").val())) {
        $('#ser_costo_data').parents('.form-group').addClass('has-error');
        return false;
    }
    $('#ser_costo_data').parents('.form-group').removeClass('has-error');
    $('#ser_costo_data').prop("readonly", true);
    $('.precio_edit').find('button').toggle();
};


propuestas.habilitaFormulario = function () {
    $('#vistaPropuestas').hide(efecto());
    $('#formCliente').show(efecto());
    $("#sgte").show(efecto());
    $("#cancel").show(efecto());
};


propuestas.deshabilitaFormulario = function () {
    $('#print').hide();
    $('#new').show(efecto());
    $('#view').show(efecto());
    $("#propuesta_generada").hide();
    $('#vistaPropuestas').show(efecto());
    $('#formCliente').hide(efecto());
    $('#formCliente').find("input").val("");
    $('#formServicio').hide(efecto());
    $('#formServicio').find("input").val("");
    $(".abajo").find("button").hide(efecto());
    $("#cli_cod").selectpicker('val', 0);
};


propuestas.buscarServicios = function () {
    $.get("/megaglobal_web_2/servicios", 'o=r', function (response) {
        renderizarHTML(response, 't-servicios', 't_servicios tbody');

        propuestas.servicios = response.data;
    });
};


propuestas.new = function () {
    $("#pro_fecha").val(today());
    $('#new').hide(efecto());
    $('#view').hide(efecto());
    propuestas.habilitaFormulario();
};

propuestas.see = function () {

};

propuestas.print = function () {


    domtoimage.toBlob(document.getElementById('propuesta_generada'))
            .then(function (blob) {
                saveAs(blob, document.title + '.png');
            });
};

propuestas.salir = function () {
    window.open('/megaglobal_web_2/', '_self');
};

propuestas.anular = function () {

};

propuestas.save = function () {

    if (propuestas.detalle.length < 1)
        return alertify.error("No se ha agregado ningún servicio");

    var data = {
        "o": "s",
        "cli_cod": $('#cli_cod').val(),
        "detalle": JSON.stringify(propuestas.detalle),
        "token": makeToken()
    };

    $.post(propuestas.url, data, function (response) {

        if (response.c > 0 || response.d > 0) {
            alertify.success("Se ha guardado con exito");
            propuestas.detalle = [];
            $('#cli_cod').resetZelect();
            propuestas.listarPropuestas();
            propuestas.detalle = [];
            renderizarHTML(propuestas, 't_detalles', 'servicios_detalles tbody');
            propuestas.deshabilitaFormulario();
        } else {
            alertify.error("Ha ocurrido un error");
            console.log(response.error);
        }
    });


};


propuestas.cancel = function () {
    if (propuestas.detalle.length > 0 || $("#ser_cod").val() != 0 || $("#cli_cod").val() != 0) {
        alertify.confirm("Estas seguro de cancelar? <br> Se eliminaran los datos escritos.", function (e) {
            if (e) {
                propuestas.detalle = [];
                renderizarHTML(propuestas, 't_detalles', 'servicios_detalles tbody');
                propuestas.deshabilitaFormulario();
            }
            ;
        });
    } else {
        propuestas.deshabilitaFormulario();
    }
};


propuestas.serv = function () {
    $("#dialog_servicios").dialog("open");
};

propuestas.export = function () {
    var i = 0;
    $('.ck:checked').each(function () {
        i = (Number($(this).val()));
    });


    var servicio = propuestas.servicios[i];
    var mod = $('#prop_mod').val();
    $('#ser_cod').val(servicio.ser_cod);
    $('#ser_descripcion_data').val(servicio.med_descripcion + " " + servicio.ser_descripcion);
    $('#ser_costoestandar_data').val(servicio[mod]);
    $('#dialog_servicios').dialog('close');
};

//eventos automaticos
$('#det_cantidad').keypress(function (event) {
    event.preventDefault();
    propuestas.calcCosto();
});

propuestas.autocompletar = function () {
    var mod = $('#prop_mod').val();

    var cod = $('#ser_cod').val();

    function filtarServicios(obj) {
        if (obj.ser_cod === (Number(this))) {
            return true;
        } else {
            return false;
        }
    }
    var c = propuestas.servicios.filter(filtarServicios, cod) || "";

    if (c.length > 0) {
        $('#ser_cod').val(c[0].ser_cod);
        $('#ser_descripcion_data').val(c[0].med_descripcion + " " + c[0].ser_descripcion);
        $('#ser_costoestandar_data').val(c[0][mod]);
        $("#det_cantidad").val("1");
        $("#det_cantidad").focus();
        propuestas.calcCosto();
    } else {
        alertify.error("Codigo erróneo");
        $('#ser_descripcion_data').val('');
        $('#ser_costoestandar_data').val('');
        $("#det_cantidad").val("");
        $('#ser_costo_data').val("");
    }
};


propuestas.calcCosto = function () {

    if (!/^([0-9])*$/.test($("#det_cantidad").val())) {
        $("#det_cantidad").parent('div').addClass('has-error');
    } else {
        var a = Number($("#det_cantidad").val());
        var b = Number($('#ser_costoestandar_data').val());
        var multi = a * b;
        $('#ser_costo_data').val(multi);
        $("#det_cantidad").parent('div').removeClass('has-error');
    }
};


propuestas.regresar = function () {
    if (propuestas.detalle.length > 0) {
        alertify.confirm("Estas seguro de regresar ? <br> Se eliminaran los detalles", function (e) {
            if (e) {
                propuestas.detalle = [];
                renderizarHTML(propuestas, 't_detalles', 'servicios_detalles tbody');
                propuestas.cargarServicios();
            }
            ;
        });
    } else {
        propuestas.cargarServicios();
    }
};


propuestas.cargarServicios = function () {

    if ($("#cli_cod").val() != 0) {
        $("#formCliente").toggle({
            easing: "easeInOutQuint",
            duration: 1000
        });
        $("#formServicio").toggle({
            easing: "easeInOutQuint",
            duration: 1000
        });

        $(".toggle").toggle();
        $("#save").toggle();

    } else {
        alertify.error("No se ha seleccionado el Cliente");
    }
};

propuestas.e = function (i) {
    propuestas.detalle.splice(i, 1);

    renderizarHTML(propuestas, 't_detalles', 'servicios_detalles tbody');
    propuestas.actualizarTotal();

};


propuestas.m = function (i) {

    var c = propuestas.detalle[i];

    $('#ser_cod').val(c.ser_cod);
    $('#ser_descripcion_data').val(c.ser_descripcion_data);
    $('#ser_costoestandar_data').val(c.monto_standar);
    $('#ser_costo_data').val(c.det_monto);
    $("#det_cantidad").val(c.det_cantidad);
    $("#det_cantidad").focus();
    propuestas.e(i);
};


propuestas.menuServicios = function () {
    context.attach('#servicios_detalles tbody .clickable-row', [
        {
            header: 'Servicios'
        },
        {
            icon: 'glyphicon-pencil',
            text: 'Editar',
            action: function () {
                $('.clickable-row').removeClass('active');
                propuestas.m(context.dataindex);
            }
        },
        {
            icon: 'glyphicon-trash',
            text: 'Eliminar',
            action: function () {
                $('.clickable-row').removeClass('active');
                propuestas.e(context.dataindex);
            }
        }
    ]);
};


propuestas.menuPropuestas = function () {
    context.attach('#propuestas tbody tr', [
        {
            header: 'Propuestas'
        },
        {
            icon: 'glyphicon-eye-open',
            text: 'Ver propuesta',
            action: function () {
                $('.clickable-row').removeClass('active');
                propuestas.verPropuesta(context.dataindex);
            }
        },
        {
            icon: 'glyphicon-refresh',
            text: 'Actualizar',
            action: function () {
                $('.clickable-row').removeClass('active');
                propuestas.listarPropuestas();
            }
        }
    ]);
};

$(document).ready(function () {
    document.title = "Propuestas | Mega Global";
    $("#t_servicios").buscador();
    //$("#det_cantidad").spinner();
    $("#prop_mod").selectpicker();
    propuestas.buscarServicios();
    propuestas.cargarClientes();
    propuestas.listarPropuestas();

    $('#servicios_detalles').activarFila();
    $('#propuestas').activarFila();

    $("#det_cantidad").TouchSpin({
        verticalbuttons: true,
        max: 10,
        stepinterval: 1,
        min: 1
    });

    context.init({
        fadeSpeed: 100,
        above: 'auto',
        preventDoubleContext: true,
        compress: false
    });

    propuestas.menuServicios();
    propuestas.menuPropuestas();


    $("#pro_fecha").datepicker();

    $('#propuestas').buscador();

    $("#dialog_servicios").dialog({
        closeText: "Cerrar",
        draggable: true,
        autoOpen: false,
        modal: true,
        title: 'Buscar Servicios',
        resizable: false,
        width: 550,
        position: {my: "top", at: "top", of: "body"},
        show: {
            effect: "drop",
            direction: "up",
            duration: 500
        },
        hide: {
            effect: "drop",
            direction: "down",
            duration: 500
        }
    });

    $('#det_cantidad').on('touchspin.on.startspin', function () {
        propuestas.calcCosto();
    });
});