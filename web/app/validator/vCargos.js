/* global Handlebars, alertify, $, esp */

var cargos = {};
cargos.lista = {};
cargos.data = {};





function ocultarBtn() {
    $('#new').hide();
//    $('#info').hide("blind", 500);

    $('#save').show();
    $('#cancel').show();
}
;

function mostrarBtn() {
    $('#new').show();
    $('#formu').dialog("close");
    $('#save').hide();
    $('#save_new').hide();
    $('#cancel').hide();
}
;

function cargarCargos() {
//    $("#car_descri").val("");
//    var p = Handlebars.compile($('#t_cargos').html());
//    $.get('/megaglobal_web_2/cargos', 'o=r', function (response) {
//        cargos.lista = response;
//        $('#cargos').hide("blind", 500);
//        $('#cargos').html(p(response));
//        $('#cargos').show("blind", 500);
//    });
    $('#cargos').DataTable().ajax.reload();

}
;

cargos.new = function () {
    ocultarBtn();
    $('#car_descri').val("");
    $("#formu").dialog({
        title: 'Crear Nuevo Cargo',
        buttons: [
            {
                text: "Guardar",
                'class': 'btn btn-success',
                click: function () {
                    save();
                }
            },
            {
                text: "Cancelar",
                'class': 'btn',
                click: function () {
                    mostrarBtn();

                }
            }
        ]
    });
    $("#formu").dialog("open");
};

cargos.salir = function () {
    window.open('/megaglobal_web_2/', '_self');
};

cargos.cancel = function () {
    mostrarBtn();
};


function save() {
    var data = {
        "o": "s",
        "car_descri": $('#car_descri').val()
    };
    if (validar_campo(['car_descri'])) {
        $.post('/megaglobal_web_2/cargos', data, function (response) {
            if (Number(response) > 0) {
                alertify.success("Se ha insertado con éxito");
                cargarCargos();
                mostrarBtn();
            } else {
                alertify.error("Ha ocurrido un error");
            }
        });
    }
}
;

  function save_new () {
    var data = {
        "o": "m",
        "car_descri": $('#car_descri').val().trim(),
        "car_cod": cargos.data.car_cod
    };

    if (validar_campo(['car_descri'])) {
        alertify.confirm("Estas seguro de Modificar el dato a <b>" + data.car_descri + "</b> ?", function (e) {
            if (e) {
                $.post('/megaglobal_web_2/cargos', data, function (response) {
                    if (Number(response) > 0) {
                        alertify.success("Se ha modificado con éxito");
                        cargarCargos();
                        mostrarBtn();
                    } else {
                        alertify.error("Ha ocurrido un error");
                    }
                });

            }
            ;
        });
    }




};

cargos.m = function (i) {
    $("#car_descri").val(i.car_descri);
    cargos.data.car_cod = i.car_cod;
    
    $("#formu").dialog({
        title: 'Modificar cargo',
        buttons: [
            {
                text: "Guardar Cambios",
                'class': 'btn btn-success',
                click: function () {
                    save_new();
                }
            },
            {
                text: "Cancelar",
                'class': 'btn',
                click: function () {
                    $(this).dialog("close");

                }
            }
        ]
    });
    $("#formu").dialog("open");
};

cargos.e = function (i) {

    var data = {
        "o": "e",
        "car_cod": i.car_cod
    };

    alertify.confirm("Estas seguro de eliminar <b>" + i.car_descri + "</b> ?", function (e) {
        if (e) {
            $.post('/megaglobal_web_2/cargos', data, function (response) {
                if (Number(response) > 0) {
                    alertify.success("Se ha eliminado con éxito");
                    cargarCargos();
                    mostrarBtn();
                } else {
                    alertify.error("Ha ocurrido un error");
                }
            });

        }
        ;
    });

};

$(document).ready(function () {

    $('.dialogo').tooltip();



    // inicializa la tabla

    $('#cargos').dataTable({
        language: esp,
        "ajax": "/megaglobal_web_2/cargos?o=r",
        "columns": [
            {"data": "car_cod"},
            {"data": "car_descri"}
        ]
    });


    //agrega el plugin de menu contextual
    $('#cargos').find("tbody").on("click", "tr", function () {

        if ($(this).hasClass("b")) {
            return false;
        } else {
            $(this).addClass("b");
            var p = Handlebars.compile($('#t_cargos').html());
            $(this).find("td:last-child").append(p({
                "car_descri": $(this).find("td:eq(1)").html(),
                "data": JSON.stringify({"car_cod": $(this).find("td:eq(0)").html(), "car_descri": $(this).find("td:eq(1)").html()})
            }));
        }

    });



// botones de eliminar y modificar
    $('#cargos').find("tbody").find("tr").mouseover(function () {
        $(this).find("td:eq(0)").html('1');
    }).mouseout(function () {
        $(this).find("td:eq(0)").html('2');
    });


    $("#formu").dialog({
        autoOpen: false,
        modal: true,
        draggable: false,
        resizable: false,
        class: 'col-md-6',
        position: {my: "top", at: "top", of: "body"},
        show: {
            effect: "drop",
            duration: 500
        },
        hide: {
            effect: "drop",
            duration: 500
        }
    });

});