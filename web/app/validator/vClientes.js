/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var clientes = {};

/* global Handlebars, alertify, $, esp, context */


/**
 * cli_cod integer NOT NULL, 
 * cli_nombre character varying(60) NOT  NULL,
 * cli_ci_ruc character varying(20) NOT NULL,
 * cli_email character varying(100) NOT NULL, 
 * cli_direccion character varying(100) NOT NULL,
 * ciu_cod integer NOT NULL,
 */

clientes.data = function () {
    $.get('/megaglobal_web_2/clientes', 'o=r', function (data) {
        //console.log(data);
        renderizarHTML(data, 't_clientes', 'clientes tbody');
        clientes.lista = data;
    });
};

clientes.agregaCiudad = function (ciudad) {
    alertify.confirm("Estas seguro guardar <b>" + ciudad + "</b> en la lista ?", function (e) {
        if (e) {
            $.post("/megaglobal_web_2/ciudad", "o=s&ciu_descri=" + ciudad, function (response) {
                if (Number(response) > 0) {
                    alertify.success("Se ha insertado con éxito");
//                    $( ".zelect" ).remove();
                    clientes.ciudades();
                } else {
                    alertify.error("Ha ocurrido un error");
                }
            });
        };
    });


};

clientes.ciudades = function () {
    $.get('/megaglobal_web_2/clientes', 'o=ciudades', function (data) {
        renderizarHTML(data, 't_ciudades', 'ciu_cod');
        $('.selectpicker').selectpicker('refresh');
//        $("#ciu_cod").zelect({
//            noResults: function (str) {
//                return "<span onclick=\"clientes.agregaCiudad('" + str + "')\">Agregar <b>" + str + "</b> la lista</span>";
//            }
//        });
    });
};


clientes.ocultarBtn = function () {
    $('#new').hide();
    $('#info').hide("drop", "right");
    $("#formu").show("drop", "left");
    $("#cancel").show();
}
;



clientes.mostrarBtn = function () {
    $(".form-group").removeClass('has-error');
    $(".input-group").removeClass('has-error');
    $('.form-control').val("");
    $('#ciu_cod').resetZelect();
    $('#new').show();
    $('#formu').hide("drop", "left");
    $('#info').show("drop", "right");
    $('#save').hide();
    $('#save_new').hide();
    $('#cancel').hide();
}
;

clientes.limpiarCampos = function () {
    $('input').val();
    $('select').val(0);
    resetStyles();
};

clientes.save_new = function () {
    var data = {
        "o": "u",
        "cli_cod": clientes.pk,
        "cli_nombre": $("#cli_nombre").val(),
        "cli_ci_ruc": $("#cli_ci_ruc").val(),
        "cli_email": $("#cli_email").val(),
        "cli_telefono": $("#cli_telefono").val(),
        "cli_direccion": $("#cli_direccion").val(),
        "ciu_cod": $("#ciu_cod").val()
    };

    if (validar_campo(['cli_nombre', 'cli_ci_ruc', 'cli_email', 'cli_direccion', 'ciu_cod', 'cli_telefono'])) {

        alertify.confirm("Estas seguro de Modificar los datos del cliente: <b>" + data.cli_nombre + "</b> ?", function (e) {
            if (e) {
                $.post('/megaglobal_web_2/clientes', data, function (response) {
                    if (Number(response) > 0) {
                        alertify.success("Se ha modificado con éxito");
                        clientes.data();
                        clientes.mostrarBtn();
                    } else {
                        alertify.error("Ha ocurrido un error");
                    }
                });

            }
            ;
        });
    }
}
;

clientes.new = function () {
    renderizarHTML({title: "Nuevo Cliente"}, 't_title', 'title');
    clientes.ocultarBtn();
    clientes.limpiarCampos();
    $('#save').show();
};

clientes.salir = function () {
    window.open('/megaglobal_web_2/', '_self');
};

clientes.cancel = function () {
    clientes.limpiarCampos();
    clientes.mostrarBtn();
};


clientes.save = function () {
    var datos = {
        "o": "c",
        "cli_nombre": $("#cli_nombre").val(),
        "cli_ci_ruc": $("#cli_ci_ruc").val(),
        "cli_email": $("#cli_email").val(),
        "cli_telefono": $("#cli_telefono").val(),
        "cli_direccion": $("#cli_direccion").val(),
        "ciu_cod": $("#ciu_cod").val()
    };

    if (validar_campo(['cli_nombre', 'cli_ci_ruc', 'cli_email', 'cli_direccion', 'ciu_cod', 'cli_telefono'])) {
        $.post('/megaglobal_web_2/clientes', datos, function (response) {
            if (Number(response) > 0) {
                alertify.success("Se ha insertado con éxito");
                clientes.data();
                clientes.mostrarBtn();
            } else {
                alertify.error("Ha ocurrido un error");
            }
        });
    }
}
;

clientes.verifica_ruc_ci = function () {
    var datos = {
        "o": "vr",
        "cli_ruc_ci": $("#cli_ci_ruc").val()
    };
    $.post('/megaglobal_web_2/clientes', datos, function (response) {
        if (response.ok) {
            $("#wCli_ci_ruc").parent('div').removeClass('has-error');
            $("#wCli_ci_ruc").hide();
        } else {
            $("#wCli_ci_ruc").parent('div').addClass('has-error');
            $("#wCli_ci_ruc").show();
        }
    });



};

clientes.m = function (i) {
    renderizarHTML({title: "Modificar datos Clientes"}, 't_title', 'title');
    clientes.pk = 0;
    var cliente = clientes.lista.data[i];
    $("#save_new").show();
    clientes.pk = cliente.cli_cod;
    $("#cli_nombre").val(cliente.cli_nombre);
    $("#cli_ci_ruc").val(cliente.cli_ci_ruc);
    $("#cli_email").val(cliente.cli_email);
    $("#cli_telefono").val(cliente.cli_telefono);
    $("#cli_direccion").val(cliente.cli_direccion);
    $("#ciu_cod").selectpicker('val', cliente.ciu_cod);
    clientes.ocultarBtn();

};

clientes.e = function (i) {

    var data = {
        "o": "d",
        "cli_cod": clientes.lista.data[i].cli_cod
    };

    alertify.confirm("Estas seguro de eliminar <b>" + clientes.lista.data[i].cli_cod + "</b> ?", function (e) {
        if (e) {
            $.post('/megaglobal_web_2/clientes', data, function (response) {
                if (Number(response) > 0) {
                    alertify.success("Se ha eliminado con éxito");
                    clientes.data();
                    clientes.mostrarBtn();
                } else {
                    alertify.error("Ha ocurrido un error");
                }
            });

        }
        ;
    });
};

clientes.ver_cliente = function (i) {
    var content = Handlebars.compile('<h3>{{ cli_nombre }}</h3><dl>\n\
    <dt>Código</dt><dd>{{ cli_cod }}</dd><dt>CI/RUC</dt><dd>{{ cli_ci_ruc }}</dd>\n\
    <dt>Dirección</dt><dd>{{ cli_direccion }}</dd><dt>Teléfono</dt>\n\
    <dd>{{ cli_telefono }}</dd><dt>Ciudad</dt><dd>{{ ciu_descri }}</dd></dl>');


    $('#info_clientes').dialog({
        autoOpen: false,
        modal: true,
        draggable: false,
        title: 'Más información',
        resizable: false,
        maxWidth: 'auto',
        buttons: [
            {
                text: "Aceptar",
                click: function () {
                    $(this).dialog("close");
                },
                class: 'btn btn-default'
            }
        ],
        position: {my: "top", at: "top", of: "body"},
        show: {
            effect: "drop",
            duration: 500,
            direction: "up"
        },
        hide: {
            effect: "drop",
            duration: 500,
            direction: "down"
        }
    });
    $('.ui-dialog-content').html(content(clientes.lista.data[i]));
    $('#info_clientes').dialog('open');
};


clientes.menuClientes = function(){
    context.attach('#clientes tbody .clickable-row', [
        {
            header: 'Clientes'
        },
        {
            icon: 'glyphicon-pencil',
            text: 'Modificar datos',
            action: function () {
                clientes.m(context.dataindex);
                $('.clickable-row').removeClass('active');
            }
        },
        {
            icon: 'glyphicon-eye-open',
            text: 'Mas informacion',
            action: function () {
              $('.clickable-row').removeClass('active');
              clientes.ver_cliente(context.dataindex);
            }
        }
    ]);
};

$(document).ready(function () {
    document.title = "Gestionar Clientes | Mega Global";
    clientes.data();
    clientes.ciudades();
    
    
    $("#clientes").buscador();
    $("#clientes").activarFila();
    $("#clientes").freezeHeader({'height': '300px'});
    
    
    
    context.init({
        fadeSpeed: 100,
        above: 'auto',
        preventDoubleContext: true,
        compress: false
    });
    
    $("#clientes").activarFila();
    clientes.menuClientes();
});