/* 
 * Copyright 2017 damian.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* global alertify */

var usuarios = {};
usuarios.emp = [];

usuarios.url = "/megaglobal_web_2/usuario";

usuarios.recupera_usuarios = function () {
    $.post(usuarios.url, {o: "listar"}, function (r) {
        //console.log(r);
        renderizarHTML(r, "t_usuarios", "usuarios");

    });
};

usuarios.recupera_empleados = function () {
    $.post(usuarios.url, {o: "emp"}, function (r) {
        //console.log(r);
        usuarios.emp = r.empleados;
        renderizarHTML(r, "t_empleados", "emp_cod");
        $('.selectpicker').selectpicker('refresh');
    });
};


usuarios.recupera_perfiles = function () {
    $.post(usuarios.url, 'o=per', function (r) {
        //console.log(r);
        renderizarHTML(r, "t_perfiles", "per_cod");
        $('.selectpicker').selectpicker('refresh');
    });
};

/**
 * Operaciones
 */

usuarios.limpiarFormulario = function () {
    resetStyles();
    $('#usuarios_lista').show(efecto());
    $('#usuarios_formu').hide(efecto());
    $('#save').show();
    $('#cancel').show();
    $('#new').hide();
    usuarios.recupera_usuarios();
    usuarios.recupera_empleados();
    usuarios.recupera_perfiles();
};


usuarios.new = function () {
    $('#usuarios_formu').show(efecto());
    $('#usuarios_lista').hide(efecto());
    $('#save').show();
    $('#cancel').show();
    $('#new').hide();
};

usuarios.cancel = function () {

    alertify.confirm('Desea cancelar la operación?<br><small>Los datos ingresados se perdederán</small>', function (e) {
       if(e)
        return usuarios.limpiarFormulario();
    });



};



usuarios.save_new_user = function () {
    $.post(usuarios.url, {o: "newuser"}, function (r) {
        console.log(r);

    });
};

usuarios.save = function ()
{   /*<_usu_user integer>,
 <_emp_cod integer>,
 <_per_cod integer>,
 <_usu_mail character varying>
 <_token_create_by json>
 <_token_ip character varying>
 */
    if (validar_campo(['usu_user', 'usu_mail', 'per_cod', 'emp_cod'])) {
        var emp_cod = usuarios.emp[$('#emp_cod').val() - 1].emp_cod;
        var j_data = {
            o: "save",
            emp_cod: emp_cod,
            usu_user: $('#usu_user').val(),
            per_cod: $('#per_cod').val(),
            usu_mail: $('#usu_mail').val()
        };

      alertify.message("Se esta guardando los datos");
        $.ajax({
            url: usuarios.url,
            method: "POST",
            data: j_data
        }).done(function (r) {
            if(r.e){
                alertify.success("Se ha guardado con éxito");
                alertify.success("Mensaje de confirmación ha sido enviado");
                usuarios.limpiarFormulario();
            }
                
        });
    }
    ;




};


$("#emp_cod").on('changed.bs.select', function (event) {
    var cod = $('#emp_cod').val();
    if (cod !== 0) {
        var data = usuarios.emp[cod - 1];
        $('#usu_user').val(normalize(data.emp_nombre + ' ' + data.emp_apellido));
    }
});

$(document).ready(function () {
    document.title = "Mantener Usuarios | Mega Global";
    usuarios.recupera_usuarios();
    usuarios.recupera_empleados();
    usuarios.recupera_perfiles();

    //usuarios.save_new_user();

});