/* global alertify, domtoimage */

/**
 * vOrdenPublicidad.js
 * @version 0.1
 * @author damian
 */

var orden = {};
orden.url = '/megaglobal_web_2/orden';
orden.datos = {};
orden.reset = 0;

orden.habilitarBtn = function () {
    $('#op-new').show();
    $('#op-view').show();
    $('#op-print').hide();
    $('#op-presentacion').show(efecto());
    $('#orden_generada').hide(efecto());
    $('.abajo').find('button').hide();
};


orden.limpiarCampos = function () {
    $('.form-control').val('');
};

orden.print = function () {
    domtoimage.toBlob(document.getElementById('orden_generada'))
            .then(function (blob) {
                saveAs(blob, document.title + $('#ord_cod').html() + '.png');
            });
};


orden.deshabilitarBtn = function () {
    $('#op-new').hide();
    $('#op-view').hide();
    $('#op-presentacion').hide(efecto());
};


orden.habilitarForm = function () {
    $('#op-presentacion').hide();
    $('#op-cabecera').show(efecto());
    $('#op-next').show();
    $('#op-cancel').show();
};

orden.next = function () {
    if (validar_campo(['ord_titulo'])) {
        $('#op-next').hide();
        $('#op-save').show();
        $('#op-cabecera').hide();
        $('#op-detalle').show(efecto());
    }
};

orden.cancel = function () {

    if (orden.datos.detalles === undefined)
        return orden.habilitarBtn();

    alertify.confirm('Desea cancelar la operación?<br><b>Se borraran los datos introducidos</b>', function (opt, str) {
        if (opt) {
            orden.datos.detalles = {};
            renderizarHTML(orden.datos, 't_ctrl_servicios', "ctrl_servicios tbody");

            $('#op-cabecera').hide();
            $('#op-detalle').hide();
            orden.limpiarCampos();
            orden.habilitarBtn();
        }
    });
};

orden.new = function () {

    orden.ingresarPropuesta();
};

orden.exit = function () {
    home();
};

orden.save = function () {

    var data = {
        "o": "save",
        "ord_titulo": $('#ord_titulo').val(),
        "ord_obs": $('#ord_descripcion').val(),
        "pro_cod": orden.datos.cabecera.pro_cod,
        "detalles": JSON.stringify(orden.datos.detalles)
    };

    $.post(orden.url, data, function (callback) {
        console.log(callback);
        if (callback.c > 0 && callback.d.length > 0) {
            alertify.success("Se ha guardado la Orden");
            alertify.message("Se generaran las cuentas a cobrar...");

            orden.datos.detalles = {};
            renderizarHTML(orden.datos, 't_ctrl_servicios', "ctrl_servicios tbody");
            $('#op-presentacion').show(efecto());
            $('#op-cabecera').hide(efecto());
            $('#op-detalle').hide(efecto());
            orden.limpiarCampos();
            orden.habilitarBtn();
        }
    });
};


orden.ingresarPropuesta = function () {
    alertify.prompt('Ingresar numero de la propuesta', "", function (o, str) {
        if (o) {

            if (!isNumber(str) || str === '') {
                alertify.alert('<span class="glyphicon glyphicon-warnig"></span> Debes indicar el numero de la Propuesta', function () {
                    orden.ingresarPropuesta();
                });
            }
            orden.recuperaPropuesta(str);

        } else {
            alertify.alert('Debes indicar el Numero de la Propuesta', function () {
                orden.ingresarPropuesta();
            });
        }

    });
};

orden.recuperaOrden = function () {
    alertify.prompt('Ingresar numero de la Orden de Publicidad', "", function (o, str) {
        if (o) {

            if (!isNumber(str) || str === '') {
                alertify.alert('<span class="glyphicon glyphicon-warnig"></span> Debes indicar el número de la Orden de Publicidad', function () {
                    orden.recuperaOrden();
                });
            }
            orden.mostrarOrden(str);
        }
    });
};


orden.recuperaPropuesta = function (i) {
    data = {
        "o": "p",
        "pro_cod": i
    };

    $.get(orden.url, data, function (response) {
        orden.datos = response;
        var c = response.cabecera || 0;
        if (c == 0) {
            alertify.alert('La Propuesta <b>N° ' + padLeft(data.pro_cod, 3) + '</b> aún no ha sido creada.');
            return false;
        }

        if (response.disponible) {
            alertify.alert('La Propuesta <b>N° ' + padLeft(data.pro_cod, 3) + '</b> ya esta siendo utilizada en otra Orden de Publicidad.');
            return false;
        }




        $('#cli_nombre').val(response.cabecera.cli_nombre);
        $('#cli_ci_ruc').val(response.cabecera.cli_ci_ruc);
        $('#ord_fecha').val(today());

        for (var i = 0; orden.datos.detalles.length > i; i++) {
            orden.datos.detalles[i].ctrl_fecha_inicio = today();
            orden.datos.detalles[i].ctrl_fecha_fin = month();
        }
        ;
        console.log(orden.datos.detalles);
        $('#pro_cod').val(padLeft(orden.datos.cabecera.pro_cod, 3));
        $('#ord_titulo').focus();
        renderizarHTML(orden.datos, 't_ctrl_servicios', "ctrl_servicios tbody");
        orden.deshabilitarBtn();
        orden.habilitarForm();
    });
};


orden.mostrarOrden = function (n) {
    alertify.message("Se recuperará la Orden #" + n);
    data = {
        "o": "r",
        "ord_cod": n
    };

    $.post(orden.url, data, function (response) {

        renderizarHTML(response, 't_detalles_orden', 'detalles_orden');
        $('#ord_cod').html('#' + padLeft(response.cabecera.ord_cod, 4));
        $('#pro_cod_').html(padLeft(response.cabecera.pro_cod, 4));
        $('#pro_fecha').html(response.cabecera.pro_fecha);
        $('#ord_titulo_').html(response.cabecera.ord_titulo);
        $('.cli_ci_ruc').html(response.cabecera.cli_ci_ruc);
        $('#ord_obs_').html(response.cabecera.ord_obs || "No hay observaciones.-");
        $('#emp_nombre_apellido').html(response.cabecera.emp_nombre_apellido);
        $('.cli_nombre').html(response.cabecera.cli_nombre);
        $('#emp_ci').html(response.cabecera.emp_ci);
    }).done(function () {
        orden.deshabilitarBtn();
        $('#orden_generada').show(efecto());
        $('#op-print').show();

        $('#op-cancel').show();



    });
};


orden.m = function (i) {

    var descri = orden.datos.detalles[i];
    orden.reset = i;
    $("#ser_data").val(descri.det_cantidad + " " + descri.ser_descripcion + " en " + descri.med_descripcion);
    $("#ctrl_fecha_inicio").val(today());
    $('#ctrl_fecha_fin').val(month());
    $("#ctrl_duracion").val(1);
    $('#ctr_ser_edit').toggle(efecto());
    $('#ctrl_servicios').toggle(efecto());
};


orden.ctrlReset = function () {
    var descri = orden.datos.detalles[orden.reset];
    $("#ser_data").val(descri.det_cantidad + " " + descri.ser_descripcion + " en " + descri.med_descripcion);

    $("#ctrl_fecha_inicio").val(today());
    $('#ctrl_fecha_fin').val(month());
    $("#ctrl_duracion").val(1);
};

orden.ctrlSave = function () {
    if (!validar_campo(['ctrl_fecha_inicio', 'ctrl_fecha_fin']))
        return false;
    orden.datos.detalles[orden.reset].ctrl_fecha_inicio = $("#ctrl_fecha_inicio").val();
    orden.datos.detalles[orden.reset].ctrl_fecha_fin = $("#ctrl_fecha_fin").val();
    renderizarHTML(orden.datos, 't_ctrl_servicios', "ctrl_servicios tbody");
    $('#ctr_ser_edit').toggle(efecto());
    $('#ctrl_servicios').toggle(efecto());
};



orden.ctrlCancel = function () {
    $("#ctr_ser_edit").find(".form-control").val(null);
    $('#ctr_ser_edit').toggle(efecto());
    $('#ctrl_servicios').toggle(efecto());
};


orden.calcularMes = function () {
    $('#ctrl_fecha_fin').val(month($('#ctrl_duracion').val().trim()));
}

orden.editFecha = function () {

    $('#ctrl_fecha_fin').prop("disabled", false);
    $('#ctrl_fecha_fin').datepicker('show');
    $('#ctrl_fecha_fin').focus();
    $('.fecha_fin_edit').find('button').toggle();
};


orden.saveFecha = function () {
    alertify.defaults.glossary.title = document.title;
    if (!validar_campo(['ctrl_fecha_fin']))
        return false;
    if (dateToMs($('#ctrl_fecha_fin').val()) < dateToMs(month())) {
        $('#ctrl_fecha_fin').parents('.form-group').addClass('has-error');
        return false;
    }

    $('#ctrl_fecha_fin').parents('.form-group').removeClass('has-error');
    $('#ctrl_fecha_fin').prop("disabled", true);
    $('#ctrl_fecha_fin').datepicker('hide');
    $('.fecha_fin_edit').find('button').toggle();
};

$(document).ready(function () {
    document.title = 'Órdenes de Publicidad | Mega Global';
    alertify.defaults.glossary.title = document.title;
    $('#ctrl_duracion').spinner();
    $('#ctrl_fecha_inicio').datepicker({minDate: new Date()});
    $('#ctrl_fecha_fin').datepicker({minDate: month()});


    $('.ui-spinner-button').on('click', function () {
        orden.calcularMes();
    });

});