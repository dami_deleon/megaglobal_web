

/* global alertify */
var acceso = {};

function muestra_mensaje(str) {
    $('#help').html(str);
    $('#help').show();
}
;



acceso.acceder = function () {
    if (!validar_campo(['user', 'password'])) {
        muestra_mensaje("Los campos de usuario y contraseña no pueden estar vacios.");
        return false;
    }
    $('#help').hide();

    var data = {};
    data.user = $('#user').val();
    data.password = $('#password').val();
    
    console.log(data);
    
    $.post('/megaglobal_web_2/login', data, function (callback) {
        console.log(data);
        if (callback.acceso) {
            location.reload();
        } else {
            $('#password').val('');
            validar_campo(['user', 'password']);
            muestra_mensaje("Usuario y/o contraseña no válidos.");
        }
    });
};

acceso.establecerCuenta = function () {
    $('#spPassword').modal('show');
};

acceso.verificar_token = function () {
    $.ajax({
        method: "POST",
        url: "/megaglobal_web_2/usuario",
        data: {"o": "vt", "token": $('#token_cod').val()}
    }).done(function (response) {
        if (!response.error) {
            console.log(response);
            window.location = (window.location + 'establecer_contrasena.html?token=' + response.token_cod);
        } else {
            alertify.alert('Código expirado o inválido');
        }
        ;
    });
};

