/* global alertify */

/**
 * vEstablecerPassword.js
 * 
 */

/*
 * 
 * url_password_error error para notificar el desarrollo
 * url_password_store link con el token
 */

var password = {};
password.url = '/megaglobal_web_2/usuario';
password.user = {};

$('#p_save').on('click', function () {
    var comparacion = $('#usu_clave_confirm').val() === $('#usu_clave').val();
    if (validar_campo(['usu_clave', 'usu_clave_confirm']) && comparacion) {
        var d = {
            o: "sp",
            usu_clave: $('#usu_clave').val(),
            usu_cod: password.user.usu_cod
        };
        $.ajax({
            url: password.url,
            data: d,
            method: "POST"
        }).done(function (response) {
            if (response.result > 0) {
                alertify.alert("Se ha creado su cuenta con éxito", function () {
                    window.location = '/megaglobal_web_2';
                });
            }
            ;

        });
    } else {
        $('#usu_clave_confirm_help').html('Las claves no coinciden');
    }

});

$('#usu_clave').keyup(function () {
    if ($(this).val() === $('#usu_clave_confirm').val() || $("usu_clave_confirm").val() === "") {
        $('#p_save').show();
        $('#usu_clave_confirm_help').html('');
    } else {
        $('#p_save').hide();
        $('#usu_clave_confirm_help').html('Las claves no coinciden');
    }
});

$('#usu_clave_confirm').keyup(function () {
    if ($(this).val() === $('#usu_clave').val() || $(this).val() === "") {
        $('#p_save').show();
        $('#usu_clave_confirm_help').html('');
    } else {
        $('#p_save').hide();
        $('#usu_clave_confirm_help').html('Las claves no coinciden');
    }
});

password.recuperar_datos = function () {
    $.ajax({
        url: password.url,
        method: "POST",
        data: {o: "vt", "token": urlParam('token')}
    }).done(function (r) {
        password.user = r;
        console.log(password.user);
        if (r.valid) {
            $('#usu_user').val(r.usu_user);
            $('#usu_mail').val(r.usu_mail);
            $('#p_save').show();
            $('#p_cancel').show();
        } else {

        }

    });
};

$(document).ready(function () {
    password.recuperar_datos();
});