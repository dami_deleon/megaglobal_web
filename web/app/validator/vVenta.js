/* 
 * Copyright 2016 damian.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* global alertify, context */

ventas = {};
ventas.url = '/megaglobal_web_2/venta';
ventas.cuentas = [];
ventas.data = [];
ventas.factura = {descripcion: []};
ventas.facturas_emitidas = [];
ventas.clientes = {};

ventas.desactivaBotones = function () {
    $('#vt-new').hide();
    $('#vt-view').hide();
    $('#vt-next').show();
};

ventas.activaBotones = function () {
    $('#vt-cabecera').show(efecto());
    $('#vt-clientes').hide();
    $('#vt-factura').hide();
    $('#vt-cancel').hide();
    $('#vt-save').hide();
    $('#vt-new').show();
    $('#vt-view').show();
};

ventas.new = function () {
    ventas.desactivaBotones();
    $('#vt-cabecera').hide(efecto());
    $('#vt-clientes').show(efecto());

};

ventas.addCliente = function (index) {
    $('#vt-cli_cod').selectpicker('val', ventas.clientes[index].cli_cod);
    ventas.desactivaBotones();
    $('#vt-cabecera').hide(efecto());
    $('#vt-clientes').show(efecto())
    ventas.searchCuentasByCliente();
};

ventas.next = function () {
    var data = [];
    var cod = [];
    $('.ck:checked').each(function () {
        cod.push(Number($(this).val()));
    });

    if (cod.length < 1)
        return alertify.alert('Se debe seleccionar al menos una cuenta');

    var _cuentas_ = ventas.data.cuentas;
    //ventas.data.cuentas = {};

    for (var i = 0; i < cod.length; i++) {
        data.push(_cuentas_[cod[i]]);
    }
    ;

    alertify.confirm("Estas seguro de generar la factura?", function (e) {
        if (e) {
            ventas.facturaDescripcion(data);
            $('#vt-next').hide();
            $('#vt-cancel').show();
            $('#vt-save').show();
        }
        ;
    });
};


ventas.cuentasPendientes = function () {
    data = {
        "o": "cuentas"
    };
    $.post(ventas.url, data, function (respuesta) {
        ventas.cuentas = respuesta;
        renderizarHTML(ventas, 't_cuentas', 'cuentas tbody');
    });
};


ventas.addCuenta = function (cabecera, detalle) {
    cabecera.detalles = detalle;
    ventas.factura.descripcion.push(cabecera);
};


ventas.cliente = function () {
    data = {
        "o": "clientes"
    };

    $.post(ventas.url, data, function (respuesta) {
        renderizarHTML(respuesta, 'vt-t_cli_cod', "vt-cli_cod");
        //$('#vt-cli_cod').zelect();
        $('#vt-cli_cod').selectpicker();
        ventas.clientes = respuesta.clientes;
        
        $('#vt-cli_cod').on('change', function (evt, item) {
            ventas.searchCuentasByCliente();
        });
    });
};


ventas.searchCuentasByCliente = function () {

    if (!validar_campo(['vt-cli_cod']))
        return $('#cuentas_cliente').hide(efecto());

    data = {
        "o": "cuentas_cliente",
        "cli_cod": $('#vt-cli_cod').val()
    };

    $.post(ventas.url, data, function (respuesta) {
        ventas.data = {"cuentas": respuesta};
        renderizarHTML(ventas.data, 'vt-t_cuentas_cliente', 'vt-cuentas_cliente tbody');
        $('#vt-cuentas_cliente').show(efecto());
    });
};


ventas.consultar_facturas = function () {
    data = {
        "o": "consultar_facturas"
    };

    $.post(ventas.url, data, function (respuesta) {

        for (var i = 0; i < respuesta.facturas_emitidas.length; i++) {

            var d = JSON.parse(respuesta.facturas_emitidas[i].ven_metadata);


            d = {
                "tal_cod": respuesta.facturas_emitidas[i].tal_cod,
                "ven_fecha": respuesta.facturas_emitidas[i].ven_fecha,
                "razon_social": d.razon_social,
                "ruc": d.ruc,
                "ven_nro_factura": respuesta.facturas_emitidas[i].ven_nro_factura
            };

            ventas.facturas_emitidas.push(d);

        }
        ;

        renderizarHTML(ventas, 'vt-t_factura_lista', 'vt-factura_lista tbody');
    });
};


ventas.facturaDescripcion = function (json) {
    for (i = 0; i < json.length; i++) {

        $.ajax({
            async: false,
            method: "POST",
            url: ventas.url,
            data: {
                "o": "descripcion_factura",
                "pro_cod": json[i].pro_cod,
                "cta_fecha_vencimiento": json[i].cta_fecha_vencimiento
            }
        }).done(function (msg) {
            ventas.addCuenta(json[i], msg);
        });

    }//fin de for



    $('#vt-clientes').hide(efecto());
    $('#vt-factura').show(efecto());
    ventas.cabecera(json);
};


ventas.cabecera = function (json) {

    $('#vt-cli_ci_ruc').val(json[0].cli_ci_ruc);
    $('#vt-cli_nombre').val(json[0].cli_nombre);
    $('#vt-cli_sector').val(json[0].cli_sector);
    $('#vt-cli_direccion').val(json[0].cli_direccion + ", " + json[0].ciu_descri);
    $('#vt-ven_fecha').val(today());

    renderizarHTML(ventas.factura, 'vt-t_descripcion', 'vt-descripcion');
    var total = 0;

    for (var i = 0; i < ventas.factura.descripcion.length; i++) {
        total += ventas.factura.descripcion[i].cta_monto_cobrar;
    };

    $('#total_pagar_num').html(formato_nro(total));
    $('#valor_parcial').html(formato_nro(total));
    $('#total_pagar_str').html(num_str(total));
    $('#iva_10').html(formato_nro(Math.round(total / 11)));
    $('#total_iva_10').html(formato_nro(Math.round(total / 11)));
    $('#iva_total').html(formato_nro(Math.round(total / 11)));
};


ventas.editCampo = function (campo) {
    $('#' + campo).prop("disabled", false);
    $('#' + campo).focus();
    $('.btn-' + campo).toggle();
};


ventas.saveCampo = function (campo) {
    if (!validar_campo([campo]))
        return false;

    $('#' + campo).parent('.form-group').removeClass('has-error');
    $('#' + campo).prop("disabled", true);
    $('.btn-' + campo).toggle();

    resetStyles();
};



ventas.save = function () {
    
    var descr = []; //variable array de los detalles;
   
   //recorre tabla de detalles
    $('#vt-descripcion').find('tr').each(function () {
        
        var t = $(this).find('td:eq(1)').html();
        var d = {"cantidad": $(this).find('td:eq(0)').html(), //lo que hay en la primera columna
            "descripcion": t.replace(/(\s\s)+/g, ""), //lo que hay en la segunda columna eliminado los dobles espacios
            "precio_unitario": $(this).find('td:eq(2)').html(),
            "exenta": $(this).find('td:eq(3)').html(),
            "iva_5": $(this).find('td:eq(4)').html(),
            "iva_10": $(this).find('td:eq(5)').html()};
        descr.push(d);//concatenando en la variable Array de la descripcion
    });

    var factura = {
        "condicion_pago": $('#vt-con_cod').data('zelect-item'),
        "razon_social": $('#vt-cli_nombre').val(),
        "ruc": $('#vt-cli_ci_ruc').val(),
        "direccion": $('#vt-cli_direccion').val(),
        "detalle": descr,
        "valor_parcial": {"iva_10": $('#valor_parcial').html(),
            "iva_5": $('#valor_parcial_5').html(),
            "exentas": $('#valor_parcial_ex').html()
        },
        "total_num": $('#total_pagar_num').html(),
        "total_str": $('#total_pagar_str').html(),
        "liquidacion_iva": {"iva_10": $('#total_iva_10').html(),
            "iva_5": $('#total_iva_5').html(),
            "total": $('#iva_total').html()
        }
    };
    detalle = [];
    for (var i = 0; i < ventas.factura.descripcion.length; i++) {
        detalle.push({
            "ord_cod": ventas.factura.descripcion[i].ord_cod,
            "pro_cod": ventas.factura.descripcion[i].pro_cod,
            "cta_cod": ventas.factura.descripcion[i].cta_cod
        });
    }
    ;

    $.ajax({
        async: false,
        method: "POST",
        url: ventas.url,
        data: {
            "o": "save",
            "con_cod": $('#vt-con_cod').val(),
            "factura": JSON.stringify(factura),
            "detalle": JSON.stringify(detalle)
        }
    }).done(function (msg) {
        if (msg.error) {
            alertify.error(msg.detail);
        } else {
            alertify.success("Factura Guardada con exito");
            alertify.confirm("Desea visualizar la factura #" + padLeft(msg.factura.ven_nro_factura, 4) + "? ", function (e) {
                ventas.ver_factura(msg.factura.ven_nro_factura, msg.factura.tal_cod);

            });
            ventas.activaBotones();
            ventas.consultar_facturas();
        }
    });
};


ventas.ver_factura = function (ven_nro_factura, tal_cod) {
    var url = "/megaglobal_web_2/#/factura?ven_nro_factura=" + ven_nro_factura + "&tal_cod=" + tal_cod;
    window.open(url, "_blank");
    ventas.activaBotones();
};


ventas.cancel = function(){
    ventas.activaBotones();
    
};


ventas.exit = function(){
    home();
};


ventas.menuCuentasPendientes = function () {
    context.attach('#cuentas tbody .clickable-row', [
        {
            header: 'Cuentas Pendientes'
        },
        {
            icon: 'glyphicon-pencil',
            text: 'Cobrar',
            action: function () {
                ventas.addCliente(context.dataindex);
                $('.clickable-row').removeClass('active');
            }
        },
        {
            icon: 'glyphicon-eye-open',
            text: 'Ver',
            action: function () {
                alertify.success("ok");
                //$('.clickable-row').removeClass('active');
//                cobros.elimMedioPago(context.dataindex);
            }
        }
    ]);
};


ventas.menuFacturaEmitida = function(){
    context.attach('#vt-factura_lista tbody .clickable-row', [
        {
            header: 'Facturas emitidas'
        },
        {
            icon: 'glyphicon-eye-open',
            text: 'Ver',
            action: function () {
                alertify.success("ok");
               
            }
        }
    ]);
};

$(document).ready(function () {
    document.title = "Ventas | Mega Global";
    alertify.defaults.glossary.title = document.title;
    ventas.cuentasPendientes();
    $("#cuentas").activarFila();
    $("#vt-factura_lista").activarFila();
     $("#vt-factura_lista").buscador();
    $("#cuentas").buscador();
    
    $("#cuentas").freezeHeader({ 'height': '300px' });
    $("#vt-factura_lista").freezeHeader({ 'height': '300px' });
    
    context.init({
        fadeSpeed: 100,
        above: 'auto',
        preventDoubleContext: true,
        compress: false
    });
    
    ventas.consultar_facturas();
    ventas.menuFacturaEmitida();
    ventas.menuCuentasPendientes();
   
    ventas.cliente();

});