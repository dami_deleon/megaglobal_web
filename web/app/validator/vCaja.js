/* 
 * Copyright 2017 damian.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/* global alertify */

var caja = {};

caja.initForm = function () {
    $('#ape_bienvenida').show(efecto());
    $('#ape_cierre').hide();
    $('#ape_apertura').hide();
    $('.btn_operativo').show();
    
    resetStyles();
};


caja.editCampo = function (campo) {
    $('#' + campo).prop("disabled", false);
    $('#' + campo).focus();
    $('.btn-' + campo).toggle();
};


caja.saveCampo = function (campo) {
    if (!validar_campo([campo]))
        return false;

    $('#' + campo).parent('.form-group').removeClass('has-error');
    $('#' + campo).prop("disabled", true);
    $('.btn-' + campo).toggle();

    resetStyles();
};


caja.habilitarFormCierre = function () {
    $('#ape_bienvenida').hide(efecto());
    $('#ape_cierre').show();
    $('#ape_cancelar').show();
    $('#ape_save_cierre').show();
    $('.btn_operativo').hide();
};


caja.habilitaFormApertura = function () {
    $('#ape_bienvenida').hide(efecto());
    $('#ape_apertura').show(efecto());
    $('#ape_cancelar').show();
    $('#ape_save_apertura').show(efecto());
    $('.btn_operativo').hide();

};


caja.apertura = function () {
    caja.habilitaFormApertura();
};

caja.saveApertura = function () {
    if (!validar_campo(['apecie_mont_inicial', 'caja_cod']))
        return alertify.error("No se ha introducido valor correcto");
    
    alertify.confirm("Los datos de la apertura se guardaran", function (e) {
 
        if (e) {

            caja.apertura_data = {
                "o": "abrir",
                "ape_mont_inicial": $("#apecie_mont_inicial").val(),
                "caja_cod": $("#caja_cod").val()
            };

            $.ajax({
                method: 'POST',
                url: "/megaglobal_web_2/caja",
                data: caja.apertura_data

            }).done(function (data) {
                alertify.success("La caja se ha abierto con éxito");
                caja.initForm();
                caja.apertura_data = {};
            }).error(function (data) {
                alertify.error("Ha ocurrido un error");
            });
        };
    });
};


caja.cierre = function () {
    caja.habilitarFormCierre();
};


$(document).ready(function () {
    $('#caja_cod').selectpicker();

    $('#apecie_mont_inicial').TouchSpin({
        verticalbuttons: true,
        max: 1000000000,
        maxboostedstep: 10000000,
        step: 10000,
        prefix: 'Gs.'
    });
});