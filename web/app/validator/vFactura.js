/* 
 * Copyright 2016 damian.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* global alertify, domtoimage */

factura = {};

factura.view = function () {
    var talonario = urlParam('tal_cod') || '';
    var fact = urlParam('ven_nro_factura') || '';

    if (talonario === '' || fact === '') {
        alertify.alert("Se ha podido encontrar la factura", function () {
            window.close();
        });
    }
    ;



    document.title = 'Factura #' + padLeft(fact, 3) + ' | Mega Global';
    $.ajax({
        method: "POST",
        url: '/megaglobal_web_2/venta',
        data: {
            "o": "ver_factura",
            "tal_cod": talonario,
            "ven_nro_factura": fact
        }
    }).done(function (msg) {
        factura.fill(msg);
    });
};


factura.exit = function () {
    window.close();
};

factura.fill = function (data) {

    data.ven_metadata = JSON.parse(data.ven_metadata);
    $('#factura_nro').html(padLeft(data.ven_nro_factura, 7));
    $('#fecha').html(data.ven_fecha);
    $('#o').attr("checked", "checked");
    if (data.con_cod == 2){
        $('#r').attr("checked", "checked");
        $('#o').removeAttr("checked");
    }
        
    $('#cond_venta').val(data.con_cod);
    $('#cli_nombre').html(data.ven_metadata.razon_social);

    $('#cli_ci_ruc').html(data.ven_metadata.ruc);
    $('#cli_direccion').html(data.ven_metadata.direccion);

    $('#valor_parcial_ex').html(data.ven_metadata.valor_parcial.exentas);
    $('#valor_parcial_5').html(data.ven_metadata.valor_parcial.iva_5);
    $('#valor_parcial').html(data.ven_metadata.valor_parcial.iva_10);
    $('#total_pagar_num').html(data.ven_metadata.total_num);
    $('#total_pagar_str').html(data.ven_metadata.total_str);
    $('#total_iva_5').html(data.ven_metadata.liquidacion_iva.iva_5);
    $('#total_iva_10').html(data.ven_metadata.liquidacion_iva.total);
    $('#iva_total').html(data.ven_metadata.liquidacion_iva.total);

    $('#tmbrado').html(data.tal_timbrado);
    $('#tal_vence').html(data.tal_vence);


    renderizarHTML(data.ven_metadata, 't_fac-descripcion', 'fac-descripcion');
};

factura.download = function () {
    domtoimage.toBlob(document.getElementById('fac-documento'))
            .then(function (blob) {
                saveAs(blob, document.title + '.png');
            });

//    function filter(node) {
//        return (node.tagName !== 'i');
//    }
//
//    domtoimage.toSvg(document.getElementById('fac-documento'), {filter: filter})
//            .then(function (svg_generated) {
//                $('#svgfactura').prop('src', svg_generated);
//        
//                var svg = $('#svgfactura').find('svg').get(0);
//                
//                
//                var pdf = new jsPDF('p', 'pt', 'a4');
//                
//                pdf.addSVG(svg, 10, 0, {
//                    scale: 72 / 96, // this is the ratio of px to pt units
//                    removeInvalid: true // this removes elements that could not be translated to pdf from the source svg
//                });
//                svgElementToPdf(svg, pdf, {
//                    scale: 72 / 96, // this is the ratio of px to pt units
//                    removeInvalid: true // this removes elements that could not be translated to pdf from the source svg
//                });
//                pdf.output('dataurlnewwindow'); // use output() to get the jsPDF buffer
//
//            });


};

factura.print = function () {

    domtoimage.toPng(document.getElementById('fac-documento')).then(
            function (dataUrl) {
                var img = new Image();
                img.src = dataUrl;

                var doc = new jsPDF();
                doc.addImage(img, 'png', 10, 0, 180, 160);
//
//                var iframe = "<iframe src='" + doc.output('datauristring') + "'></iframe>";
//
//                var x = window.open();
//                x.document.open();
//                x.document.write(iframe);
//                x.document.close();

                doc.output('dataurlnewwindow'); //abrir pdf generado en una nueva ventana
                //doc.save(document.title + '.pdf'); //para guardar pdf generado
            })
            .catch(function (error) {
                alertify.error('Error!', error);
            });


//var doc = new jsPDF();
//var specialElementHandlers = {
//    '#editor': function(element, renderer){
//        return true;
//    }
//};
//
//doc.fromHTML($('#fac-documento').get(0), 15, 15, {
//    'width' : 850,
//    'elementHandlers': specialElementHandlers
//});

//    doc.output('dataurlnewwindow'); //abrir pdf generado en una nueva ventana

    alertify.message("Generando PDF...");


};

$(document).ready(function () {
    factura.view();

});