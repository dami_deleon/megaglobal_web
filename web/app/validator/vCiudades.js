/* global Handlebars, alertify, $, esp */

var ciudades = {};
ciudades.lista = {};
ciudades.data = {};
ciudades.url = '/megaglobal_web_2/ciudad';




ciudades.ocultarBtn = function() {
    $('#new').hide();
//    $('#info').hide("blind", 500);

    $('#save').show();
    $('#cancel').show();
}
;

ciudades.mostrarBtn = function() {
    $('#new').show();
    $('#formu').dialog("close");
    $('#save').hide();
    $('#save_new').hide();
    $('#cancel').hide();
}
;

 ciudades.cargarCiudades = function() {
//    $("#car_descri").val("");
//    var p = Handlebars.compile($('#t_cargos').html());
//    $.get('/megaglobal_web_2/cargos', 'o=r', function (response) {
//        cargos.lista = response;
//        $('#cargos').hide("blind", 500);
//        $('#cargos').html(p(response));
//        $('#cargos').show("blind", 500);
//    });
    $('#ciudades').DataTable().ajax.reload();

}
;

ciudades.new = function () {
    ciudades.ocultarBtn();
    $('#car_descri').val("");
    $("#formu").dialog({
        title: 'Crear Nuevo Ciudad',
        buttons: [
            {
                text: "Guardar",
                'class': 'btn btn-success',
                click: function () {
                    ciudades.save();
                }
            },
            {
                text: "Cancelar",
                'class': 'btn',
                click: function () {
                    ciudades.mostrarBtn();

                }
            }
        ]
    });
    $("#formu").dialog("open");
};

ciudades.salir = function () {
    window.open('/megaglobal_web_2/', '_self');
};

ciudades.cancel = function () {
    ciudades.mostrarBtn();
};


ciudades.save = function() {
    var data = {
        "o": "s",
        "ciu_descri": $('#ciu_descri').val()
    };
    if (validar_campo(['ciu_descri'])) {
        $.post(ciudades.url, data, function (response) {
            if (Number(response) > 0) {
                alertify.success("Se ha insertado con éxito");
                ciudades.cargarCiudades();
                ciudades.mostrarBtn();
            } else {
                alertify.error("Ha ocurrido un error");
            }
        });
    }
}
;

ciudades.save_new = function () {
    var data = {
        "o": "m",
        "ciu_descri": $('#ciu_descri').val(),
        "ciu_cod": ciudades.data.ciu_cod
    };

    if (validar_campo(['ciu_descri'])) {
        alertify.confirm("Estas seguro de Modificar el dato a <b>" + data.ciu_descri + "</b> ?", function (e) {
            if (e) {
                $.post(ciudades.url, data, function (response) {
                    if (Number(response) > 0) {
                        alertify.success("Se ha modificado con éxito");
                        ciudades.cargarCiudades();
                        ciudades.mostrarBtn();
                    } else {
                        alertify.error("Ha ocurrido un error");
                    }
                });

            }
            ;
        });
    }
};

ciudades.m = function (i) {
    $("#ciu_descri").val(i.ciu_descri);
    ciudades.data.ciu_cod = i.ciu_cod;

    $("#formu").dialog({
        title: 'Modificar cargo',
        buttons: [
            {
                text: "Guardar Cambios",
                'class': 'btn btn-success',
                click: function () {
                    ciudades.save_new();
                }
            },
            {
                text: "Cancelar",
                'class': 'btn',
                click: function () {
                    $(this).dialog("close");

                }
            }
        ]
    });
    $("#formu").dialog("open");
};

ciudades.e = function (i) {

    var data = {
        "o": "e",
        "ciu_cod": i.ciu_cod
    };

    alertify.confirm("Estas seguro de eliminar <b>" + i.ciu_descri + "</b> ?", function (e) {
        if (e) {
            $.post(ciudades.url, data, function (response) {
                if (Number(response) > 0) {
                    alertify.success("Se ha eliminado con éxito");
                    cargarCargos();
                    mostrarBtn();
                } else {
                    alertify.error("Ha ocurrido un error");
                }
            });

        }
        ;
    });

};

$(document).ready(function () {
       
      // inicializa la tabla

    $('#ciudades').DataTable({
        language: esp,
        "ajax": ciudades.url+"?o=r",
        "columns": [
            {"data": "ciu_cod"},
            {"data": "ciu_descri"}
        ]
    });


    //agrega el plugin de menu contextual
    $('#ciudades').find("tbody").on("click", "tr", function () {

        if ($(this).hasClass("b")) {
            return false;
        } else {
            $(this).addClass("b");
            var p = Handlebars.compile($('#t_ciudades').html());
            $(this).find("td:last-child").append(p({
                "ciu_descri": $(this).find("td:eq(1)").html(),
                "data": JSON.stringify({"ciu_cod": $(this).find("td:eq(0)").html(), "ciu_descri": $(this).find("td:eq(1)").html()})
            }));
        }

    });



// botones de eliminar y modificar
    $('#cargos').find("tbody").find("tr").mouseover(function () {
        $(this).find("td:eq(0)").html('1');
    }).mouseout(function () {
        $(this).find("td:eq(0)").html('2');
    });


    $("#formu").dialog({
        autoOpen: false,
        modal: true,
        draggable: false,
        resizable: false,
        class: 'col-md-6',
        position: {my: "top", at: "top", of: "body"},
        show: {
            effect: "drop",
            duration: 500
        },
        hide: {
            effect: "drop",
            duration: 500
        }
    });
    
    

});
