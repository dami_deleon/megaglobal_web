/* 
 * Copyright 2016 damian.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* global alertify */

var servicios = {};
servicios.url = "/megaglobal_web_2/servicios";

servicios.data = function () {
    $.get(servicios.url, 'o=r', function (response) {
        renderizarHTML(response, 't-servicios', 'servicios tbody');
        servicios.lista = response.data;
    });
};

servicios.ocultarBtn = function () {
    $('#new').hide();
    $('#info').hide("drop", "right");
    $("#formu").show("drop", "left");
    $("#cancel").show();
};

servicios.mostrarBtn = function () {
    $('#new').show();
    $('#formu').hide("drop", "left");
    resetStyles();
    $('#info').show("drop", "right");
    $('#save').hide();
    $('#save_new').hide();
    $('#cancel').hide();
};

servicios.limpiarCampos = function () {
    resetStyles();
    $('input').each(function () {
        $(this).val("");
    });
    $('#med_cod').resetZelect();
};

servicios.cancel = function () {
    servicios.limpiarCampos();
    servicios.mostrarBtn();
};

servicios.new = function () {
    renderizarHTML({title: "Nuevo Servicio"}, 't_title', 'title');
    servicios.ocultarBtn();
    $('#save').show();
};

servicios.view = function () {

}

servicios.salir = function () {
    window.open('/megaglobal_web_2/', '_self');
};



servicios.m = function (i) {
    renderizarHTML({title: "Modificar Servicio"}, 't_title', 'title');
    servicios.pk = servicios.lista[i].ser_cod;
    $("#ser_descripcion").val(servicios.lista[i].ser_descripcion);
    $("#ser_costo_mensual").val(servicios.lista[i].ser_costo_mensual);
    $("#ser_costo_unitario").val(servicios.lista[i].ser_costo_unitario);
    $("#med_cod").val(servicios.lista[i].med_cod);
    $("#med_cod").zelectItem(servicios.lista[i].med_descripcion);
    servicios.ocultarBtn();
    $("#save_new").show();
};

servicios.e = function (i) {

    var datos = {
        "o": "d",
        "ser_cod": servicios.lista[i].ser_cod
    };
    alertify.confirm("Estas seguro de eliminar <b>" + servicios.lista[i].ser_descripcion + "</b> de " + servicios.lista[i].med_descripcion + " ?", function (e) {
        if (e) {
            $.post(servicios.url, datos, function (response) {
                if (Number(response) > 0) {
                    alertify.success("Se ha eliminado con éxito");
                    servicios.data();
                } else {
                    alertify.error("Ha ocurrido un error");
                }
            });

        }
        ;
    });
};

servicios.save = function () {
    var datos = {
        "o": "c",
        "ser_descripcion": $("#ser_descripcion").val(),
        "ser_costo_mensual": $("#ser_costo_mensual").val(),
        "ser_costo_unitario": $("#ser_costo_unitario").val(),
        "med_cod": $("#med_cod").val()
    };

    if (validar_campo(['ser_descripcion', 'ser_costo_mensual', 'ser_costo_unitario', 'med_cod'])) {
        alertify.confirm("Esta seguro de guardar el nuevo Servicio?", function (e){
            if (e) {
                $.post(servicios.url, datos, function (response) {
                    if (Number(response) > 0) {
                        alertify.success("Se ha guardado exitosamente");
                        servicios.data();
                        servicios.mostrarBtn();
                        servicios.limpiarCampos();
                    } else {
                        alertify.error("Ha ocurrido un error");
                    }
                });
            }
        });
    } else {
        alertify.error("Revise los campos antes de Guardar");
    }
};


servicios.save_new = function () {


    if (validar_campo(['ser_descripcion', 'ser_costo_mensual', 'ser_costo_unitario', 'med_cod'])) {

        var datos = {
            "o": "u",
            "ser_cod": servicios.pk,
            "ser_descripcion": $("#ser_descripcion").val(),
            "ser_costo_mensual": $("#ser_costo_mensual").val(),
            "ser_costo_unitario": $("#ser_costo_unitario").val(),
            "med_cod": $("#med_cod").val()
        };

        alertify.confirm("Esta seguro de guardar el nuevo Servicio?", function (e){
            if (e) {
                $.post(servicios.url, datos, function (response) {
                    if (Number(response) > 0) {
                        alertify.success("Se han guardado los cambios");
                        servicios.data();
                        servicios.mostrarBtn();
                        servicios.limpiarCampos();
                    } else {
                        alertify.error("Ha ocurrido un error");
                    }
                });
            }
        });


    } else {
        alertify.error("Revise los campos antes de Guardar");
    }
};


servicios.menuServicios = function(){
    context.attach('#servicios tbody .clickable-row', [
        {
            header: 'Propuestas'
        },
        {
            icon: 'glyphicon-pencil',
            text: 'Modificar servicio',
            action: function () {
                $('.clickable-row').removeClass('active');
                servicios.m(context.dataindex);
            }
        },
        {
            icon: 'glyphicon-trash',
            text: 'Eliminar',
            action: function () {
                $('.clickable-row').removeClass('active');
                servicios.e(context.dataindex);
            }
        }
    ]);
};

$(document).ready(function () {
    servicios.data();
    $("#med_cod").zelect();
    $("#servicios").buscador();

    $('.help-block').hide();

    $("#ser_costo_mensual").TouchSpin({
        verticalbuttons: true,
        max:10000000,
        prefix: 'Gs.'
    });
    
    $("#ser_costo_unitario").TouchSpin({
        verticalbuttons: true,
        max:10000000,
        prefix: 'Gs.'
    });
    
    $("#ser_costo_mensual").on('touchspin.on.startspin', function(){
        var suma = Math.round((Number($("ser_costo_mensual").val()))/4);
        $("#ser_costo_unitario").val(suma);
    });

    //aparecer las ayudas del formulario
    $(".form-control").focus(function () {
        var help = $(this).attr("aria-describedby");
        $('.help-block').hide(efecto());
        $('#' + help).show(efecto());
    });
    
    context.init({
        fadeSpeed: 100,
        above: 'auto',
        preventDoubleContext: true,
        compress: false
    });
    
    servicios.menuServicios();
});


