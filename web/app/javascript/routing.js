/**
 *      routing.js 
 *      @version: v0.3;
 *      @author: Damián De León
 *      @email: damideleon97@gmail.com
 */

/**
 *  Cómo usar / How to use
 *  <div id="vista"></div>
 *  <script type="text/javascript">
 *   Routing.when({
 *       path : "/",
 *       template : "template/home.html"
 *   }).other({
 *       template : "template/error.html"
 *   }).default({
 *       path : "/",
 *       template : "template/home.html"
 *   }).on();
 *	</script>
 */

/*global $, Routing, window*/

var Routing = {};

Routing.url = {"when": []};

Routing.default = function (_routes_) {
    Routing.url.default = _routes_;
    return Routing;
};

Routing.when = function (_routes_) {
	Routing.url.when.push(_routes_);
	return Routing;
};

Routing.other = function (_routes_) {
	Routing.url.other = _routes_;
	return Routing;
};

Routing.redirect = function(url){
    var u = window.location.hash.slice(1).split('?');
    return url.path === u[0];
};

Routing.cambiar_rutas = function () {
    
    if (window.location.hash.slice(1) === ''){
        $("#vista").load(Routing.url.default.template);
        return false;
    }
    
    var rutas = Routing.url.when.filter(Routing.redirect);
	
    if(rutas.length > 0){
        $("#vista").load(rutas[0].template);
        return false;
    } else {
        $("#vista").load(Routing.url.other.template);
        return false;
    }
	
};

Routing.on = function(){
  Routing.cambiar_rutas();  
};

window.addEventListener('hashchange', function() {
	Routing.cambiar_rutas();
});	


