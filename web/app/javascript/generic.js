if (!String.prototype.format) {
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined' ? args[number] : match;
        });
    };
}
;

/**
 * 
 * @param {type} nr Numaro
 * @param {type} n Veces
 * @param {type} str Caracter
 * @returns {String}
 */
function padLeft(nr, n, str) {
    return Array(n - String(nr).length + 1).join(str || '0') + nr;
}


/**
 * @param {Array} campos Escribir los <b>id</b>'s de los campos a verificar
 * @return {boolean} Retorna <b>true</b> si todos los campos han sido llenados
 * @example <pre>validar_campo(["campo1", "campo2", "campoN"])</pre>
 * @author dami_deleon
 */
/*global $, Handlebars, alertify*/
function validar_campo(campos) {
    var valido = true;
    var totalCampos = campos.length;
    for (var i = 0; i < totalCampos; i++) {
        if ($("#" + campos[i]).val().trim() === '' || $("#" + campos[i]).val() === "0" || $("#" + campos[i]).val() == undefined) {
            $("#" + campos[i]).parents('.form-group').addClass('has-error has-feedback');
            //$("#" + campos[i]).parents('.form-group').find('.form-control').after('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>').removeClass('glyphicon-ok');
            valido = false;
        } else {
            $("#" + campos[i]).parents('.form-group').removeClass('has-error').addClass('has-success');
            //$("#" + campos[i]).parents('.form-group').find('.form-control-feedback').removeClass('glyphicon-remove').addClass('glyphicon-ok');
        }
    }
    return valido;
}


function resetStyles() {
    $('.form-group').removeClass('has-error has-success');
    $('.form-group').find('.form-control-feedback').remove();
}
;
/**
 * Fecha actual
 * @returns {String} Fecha actual
 */
function today() {
    var d = new Date();
    return padLeft(d.getDate(), 2) + "/" + (d.getMonth() + 1) + "/" + d.getFullYear();
}
/**
 * Calcular fecha despues de n meses
 * @default 1
 * @param {int} n Meses
 * @returns {String}
 */
function month(n) {
    var factor = n || 1;
    var vig = 2592000000 * factor;
    var fa = today().split("/");
    var f_i = Date.parse(fa[2] + "/" + fa[1] + "/" + fa[0]);
    var fh = new Date;
    fh.setTime(f_i + vig);
    return (padLeft(fh.getDate(), 2) + "/" + (fh.getMonth() + 1) + "/" + fh.getFullYear());
}
;


var esp = {
    "sProcessing": "Procesando...",
    "sLengthMenu": "Mostrando _MENU_ registros",
    "sZeroRecords": "No se encontraron resultados",
    "sEmptyTable": "Ningún dato disponible en esta tabla",
    "sInfo": "_START_ / _END_ de  _TOTAL_",
    "sInfoEmpty": "",
    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix": "",
    "sSearch": "Buscar:",
    "sUrl": "",
    "sInfoThousands": ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
};

$.datepicker.regional['es'] = {
    closeText: 'Cerrar',
    prevText: '<Ant',
    nextText: 'Sig>',
    currentText: 'Hoy',
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
    dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
    dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
};
$.datepicker.setDefaults($.datepicker.regional['es']);

/**
 * 
 * @param {JSON} datos
 * @param {String} idPlantilla
 * @param {String} idDestino
 * @returns {void}
 */
function renderizarHTML(datos, idPlantilla, idDestino) {
    $('#' + idDestino).html('');
    var p = Handlebars.compile($('#' + idPlantilla).html());
    $('#' + idDestino).html(p(datos));
}
;

//incremertar valor +1
Handlebars.registerHelper("inc", function (value, options)
{
    return parseInt(value) + 1;
});

//devuelve un mes despues de una fecha
Handlebars.registerHelper("mes", function () {
    return month();
});

//devuelve fecha de actual
Handlebars.registerHelper("toDay", function () {
    return today();
});

//formatea numeros
Handlebars.registerHelper("num_fmt", function (valor) {
    return formato_nro(valor);
});

//completa numeros con 0's (ceros) a la izquierda
Handlebars.registerHelper("pad_nums", function (valor) {
    return padLeft(valor, 4);
});

Handlebars.registerHelper("str_nums", function (valor) {
    return num_str(valor);
});

/**
 * Formatea numeros
 * 1000 -> 1.000.-
 * @param {Number} valor
 * @returns {String}
 */

function formato_nro(valor) {
    var nums = new Array();
    var simb = ".";
    valor = valor.toString();
    valor = valor.replace(/\D/g, "");
    nums = valor.split("");
    var long = nums.length - 1;
    var patron = 3;
    var prox = 2;
    var res = "";

    while (long > prox) {
        nums.splice((long - prox), 0, simb);
        prox += patron;
    }

    for (var i = 0; i <= nums.length - 1; i++) {
        res += nums[i];
    }

    return res.replace(/(\s\s)+/g) + '.-';
}

/**
 * 
 * @returns {efects}
 */
function efecto() {
    return {
        easing: "easeInOutQuint",
        duration: 1000
    };
}

function isNumber(int) {
    return /^([0-9])*$/.test(int);
}

function dateToMs(date) {
    var fa = date.split("/");
    var f_i = Date.parse(fa[2] + "/" + fa[1] + "/" + fa[0]);
    return f_i;
}
/**
 * Retornar a la pagina principal
 * @returns {undefined}
 */
function home() {
    alertify.confirm("¿Estás seguro de Salir?<br>Si has ingresado datos se perderán.", function (e) {
        if (e) {
            window.location = '/megaglobal_web_2/';
        }
    });
}
;


var o = new Array("diez ", "once ", "doce ", "trece ", "catorce ", "quince ",
        "dieciséis ", "diecisiete ", "dieciocho ", "diecinueve ",
        "veinte ", "veintiuno ", "veintidós ", "veintitrés ",
        "veinticuatro ", "veinticinco ", "veintiséis ", "veintisiete ",
        "veintiocho ", "veintinueve ");
var u = new Array("", "uno", "dos", "tres", "cuatro", "cinco", "seis", "siete",
        "ocho", "nueve");
var d = new Array("", "", "", "treinta", "cuarenta", "cincuenta", "sesenta",
        "setenta", "ochenta", "noventa");
var c = new Array("", "ciento", "doscientos", "trescientos", "cuatrocientos",
        "quinientos", "seiscientos", "setecientos", "ochocientos",
        "novecientos");
function num_str(n) {
    n = parseFloat(n).toFixed(2); /*se limita a dos decimales, no sabía que existía toFixed() :)*/
    var p = n.toString().substring(n.toString().indexOf(".") + 1); /*decimales*/
    var m = n.toString().substring(0, n.toString().indexOf(".")); /*número sin decimales*/
    var m = parseFloat(m).toString().split("").reverse(); /*tampoco que reverse() existía :D*/
    var t = "";
    /*Se analiza cada 3 dígitos*/
    for (var i = 0; i < m.length; i += 3)
    {
        var x = t;
        /*formamos un número de 2 dígitos*/
        var b = m[i + 1] != undefined ? parseFloat(m[i + 1].toString() + m[i].toString()) : parseFloat(m[i].toString());
        /*analizamos el 3 dígito*/
        t = m[i + 2] != undefined ? (c[m[i + 2]] + " ") : "";
        t += b < 10 ? u[b] : (b < 30 ? o[b - 10] : (d[m[i + 1]] + (m[i] == '0' ? "" : (" y " + u[m[i]]))));
        t = t == "ciento cero" ? "cien" : t;
        if (2 < i && i < 6)
            t = t == "uno" ? "mil " : (t.replace("uno", "un") + " mil ");
        if (5 < i && i < 9)
            t = t == "uno" ? "un millón " : (t.replace("uno", "un") + " millones ");
        t += x;
        //t=i<3?t:(i<6?((t=="uno"?"mil ":(t+" mil "))+x):((t=="uno"?"un millón ":(t+" millones "))+x));
    }

    //t += " con " + p + "/100";
    /*correcciones*/
    t = t.replace("  ", " ");
    //t = t.replace(" ", "");
    //t=t.replace("ciento y","cien y");
    //alert("Numero: "+n+"\nNº Dígitos: "+m.length+"\nDígitos: "+m+"\nDecimales: "+p+"\nt: "+t);
    //document.getElementById("esc").value=t;
    return t + '.-';
}
/**
 * 
 * @param {type} name
 * @returns {Array|urlParam.results|Number}
 */

function urlParam(name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results == null) {
        return null;
    } else {
        return results[1] || 0;
    }
}
;
//Sobreescribe tema de Alertify
alertify.defaults.transition = "slide";
alertify.defaults.theme.ok = "btn btn-success";
alertify.defaults.theme.cancel = "btn btn-danger";
alertify.defaults.theme.input = "form-control";
alertify.defaults.glossary.title = document.title;
alertify.defaults.glossary.ok = 'Aceptar';
alertify.defaults.glossary.cancel = 'Cancelar';
context.dataindex = 0;


(function ($) {
    $.fn.selectpicker.defaults = {
        noneSelectedText: 'No hay selección',
        noneResultsText: 'No hay resultados {0}',
        countSelectedText: 'Seleccionados {0} de {1}',
        maxOptionsText: ['Límite alcanzado ({n} {var} max)', 'Límite del grupo alcanzado({n} {var} max)', ['elementos', 'element']],
        multipleSeparator: ', ',
        selectAllText: 'Seleccionar Todos',
        deselectAllText: 'Desmarcar Todos'
    };
})(jQuery);

//probando ws

function makeToken()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_+=";

    for( var i=0; i < 30; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}
