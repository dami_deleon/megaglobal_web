/**
 * $('#tabla_id').buscador();
 * 
 * @param {Object} opts
 * @returns {void}
 * @author "damideleon97@gmail.com"
 */


jQuery.fn.buscador = function (opts) {
    var placeholder = "Buscar..." || opts.placeholder;
    var icon = '<span class = "icon-search"></span>' || opts.icon;

    var id = $(this[0]).attr("id");
    var contenedor = '<div class="filter_container" id="' + id + '_container"><div class="table_conenedor" id="' + id + '_contenedor"> </div> <div id="bottom_anchor"></div></div>';
    var buscador = '<div class="form-group">' +
            '<div class="input-group">' +
            '<span class="input-group-addon">' + icon + '</span >' +
            '<input type="text" class="form-control" placeholder="' + placeholder + '" id="filter_' + id + '"></div>' +
            '</div>';
    //agrega contenedor
    $(this[0]).before(buscador);
    $(this[0]).after(contenedor);
    $(this[0]).appendTo('#' + id + '_contenedor');
    
    
    $('#filter_' + id).on('keyup', function () {

        var tableReg = document.getElementById(id);
        var searchText = $(this).val().toLowerCase();
        var cellsOfRow = "";
        var found = false;
        var compareWith = "";
        // Recorremos todas las filas con contenido de la tabla
        for (var i = 1; i < tableReg.rows.length; i++) {
            cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
            found = false;
            // Recorremos todas las celdas
            for (var j = 0; j < cellsOfRow.length && !found; j++) {
                compareWith = cellsOfRow[j].innerText.toLowerCase();
                // Buscamos el texto en el contenido de la celda
                if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1)) {
                    found = true;
                }
            }
            if (found) {
                tableReg.rows[i].style.display = '';
            } else {
                // si no ha encontrado ninguna coincidencia, esconde la
                // fila de la tabla
                tableReg.rows[i].style.display = 'none';
            }
        }


    });



};


