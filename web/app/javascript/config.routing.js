/*global Routing*/
Routing.when({
        path: "/servicios",
        template: "app/pages/servicios.jsp"
    }).when({
        path: "/reporte",
        template: "app/pages/reporte.html"
    }).when({
        path: "/orden",
        template: "app/pages/orden_publicidad.jsp"
    }).when({
        path: "/cargos",
        template: "app/pages/cargos.jsp"
    }).when({
        path: "/cobros",
        template: "app/pages/cobros.jsp"
    }).when({
        path: "/clientes",
        template: "app/pages/clientes.jsp"
    }).when({
        path: "/apertura_cierre",
        template: "app/pages/caja.jsp"
    }).when({
        path: "/servicios_vigentes",
        template: "app/pages/servicios_vigentes.jsp"
    }).when({
        path: "/usuarios",
        template: "app/pages/usuarios.jsp"
    }).when({
        path: "/empleados",
        template: "app/pages/venta.jsp"
    }).when({
        path: "/mis_datos",
        template: "app/pages/mis_datos.jsp"
    }).when({
        path: "/passwordchange",
        template: "app/pages/passwordchange.jsp"
    }).when({
        path: "/ventas",
        template: "app/pages/venta.jsp"
    }).when({
        path: "/recibo",
        template: "app/pages/recibo.jsp"
    }).when({
        path: "/ciudades",
        template: "app/pages/ciudades.html"
    }).when({
        path: "/propuesta",
        template: "app/pages/propuestas.jsp"
    }).when({
        path: "/factura",
        template: "app/pages/factura.jsp"
    }).when({
        path: "/ws",
        template: "app/pages/websocket.jsp"
    }).other({
        template: "app/pages/error.jsp"
    }).default({
        template: "menu.html"
    }).on();