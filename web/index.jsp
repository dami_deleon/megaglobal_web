
<%-- 
    Document   : index
    Created on : 12-jun-2016, 11:26:22
    Author     : Damián
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title>Mega Global</title>
        <link href="app/font.css" rel="stylesheet" type="text/css"/>
        <link href="app/stylesheet/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="app/stylesheet/theme.min.css" rel="stylesheet" type="text/css"/>        
        <link href="app/stylesheet/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
        <link href="app/stylesheet/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="app/stylesheet/alertify.css" rel="stylesheet" type="text/css"/>
        <link href="app/stylesheet/ajs.bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="app/stylesheet/bootstrap-select.css" rel="stylesheet" type="text/css"/>
        <link href="app/stylesheet/context.standalone.css" rel="stylesheet" type="text/css"/>
        <link href="app/stylesheet/contextmenu.css" rel="stylesheet" type="text/css"/>
        <link href="app/stylesheet/jquery.bootstrap-touchspin.min.css" rel="stylesheet" type="text/css"/>
        <link href="app/stylesheet/styles.css" rel="stylesheet" type="text/css"/>
        <script src="app/javascript/jquery-1.11.3.min.js" type="text/javascript"></script>
        <script src="app/javascript/jquery-ui.min.js" type="text/javascript"></script>
        <script src="app/javascript/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="app/javascript/bootstrap.min.js" type="text/javascript"></script>
        <script src="app/javascript/bootstrap-select.js" type="text/javascript"></script>
        <script src="app/javascript/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
        <script src="app/javascript/routing.js" type="text/javascript"></script>
        <script src="app/javascript/handlebars-v2.0.0.js" type="text/javascript"></script>
        <script src="app/javascript/alertify.min.js" type="text/javascript"></script>
        <script src="app/javascript/jquery.freezeheader.js" type="text/javascript"></script>
        <script src="app/javascript/dataTables.bootstrap.min.js" type="text/javascript"></script>
        <script src="app/javascript/buscador.js" type="text/javascript"></script>
        <script src="app/javascript/activarFila.js" type="text/javascript"></script>
        <script src="app/javascript/jspdf.min.js" type="text/javascript"></script>
        <script src="app/javascript/png_support/png.js" type="text/javascript"></script>
        <script src="app/javascript/png_support/zlib.js" type="text/javascript"></script>
        <script src="app/javascript/html2pdf.js" type="text/javascript"></script>
        <script src="app/javascript/dom-to-image.js" type="text/javascript"></script>
        <script src="app/javascript/addhtml.js" type="text/javascript"></script>
        <script src="app/javascript/from_html.js" type="text/javascript"></script>
        <script src="app/javascript/FileSaver.min.js" type="text/javascript"></script>
        <script src="app/javascript/split_text_to_size.js" type="text/javascript"></script>
        <script src="app/javascript/svgToPdf.js" type="text/javascript"></script>
        <script src="app/javascript/activarFila.js" type="text/javascript"></script>
        <script src="app/javascript/zelect.js" type="text/javascript"></script>
        <script src="app/javascript/context.modify.js" type="text/javascript"></script>
        <script src="app/javascript/moment.min.js" type="text/javascript"></script>
        <script src="app/javascript/moment.local.min.js" type="text/javascript"></script>

        <script src="app/javascript/generic.js" type="text/javascript"></script>

        <%
            HttpSession sesionActiva = request.getSession();
            try {
                if (sesionActiva.getAttribute("session") == null) {

                    out.println("<script type=\"text/javascript\"> $(document).ready(function(){ "
                            + "if(window.location === '/megaglobal_web_2/index.jsp'){"
                            + "window.location = '/megaglobal_web_2/#/home';} "
                            + "$('body').load('login.jsp')}); </script>");
                } else {
                    out.println("<div class=\"container-fuid fondo\">");
                    out.println("<div class=\"dialogo\" id=\"vista\"></div>");
                    out.println("</div>");
                    out.println("<script src=\"app/javascript/config.routing.js\" type=\"text/javascript\"></script>");
                }
            } catch (Exception a) {
                response.sendRedirect("/megaglobal_web_2/login");
            }
        %>
        <script type="text/javascript">
            function compruebaSesion() {
                $.post('/megaglobal_web_2/login', function (data) {
                });
            }
        </script>

    </head>
    <body onunload="compruebaSesion()">

    </body>
</html>
