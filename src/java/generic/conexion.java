/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generic;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;


/**
 *
 * @author Damián
 */
public final class conexion {

    private Connection conexion = null; //variable de tipo conexion que recibirá el estado de la misma//
    private final String bd = "bd_megaglobal_web"; //varible a la cual se le asignará en nombre de la base de datos con la que nos deseamos conectar//
    private final String user = "postgres";//varible a la cual se le asignará en nombre del usuario que se conectará a la base de datos//
    private final String pass = "damian"; //varible a la cual se le asignará la contraseña del usuaio que se conecta a la base de datos//

    private final String controlador = "org.postgresql.Driver";//driver jdbc utilizado//
    private final String url = "jdbc:postgresql://localhost:5432/" + bd;

    public Connection conectar() {
        try {
            
            Class.forName(controlador);
            conexion = DriverManager.getConnection(url, user, pass);

        } catch (ClassNotFoundException | SQLException e) {
            //System.out.println(e.getMessage());
        }
        return conexion;
    }

    public void desConnection() {
        try {
            conexion.close();
        } catch (SQLException ex) {
            System.out.println("Problemas al Intentar Desconectar la Sesion " + ex.getMessage());
        }
    }
    
    public PreparedStatement prepararSQL(String sql){
        try {
            return conectar().prepareStatement(sql);
        } catch (SQLException ex) {
            System.err.println("Error en preparar el estamento \n" + ex);
        }
        return null;
    }
}
