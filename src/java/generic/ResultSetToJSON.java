/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generic;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Damián
 */
public class ResultSetToJSON {

    /**
     * Método que convierte un ResultSet a JSONArray
     * <PRE> toJSONArray(res);</PRE>
     *
     * @param res java.sql.ResultSet
     * @return JSONArray Array con el resulado del query
     *
     */
    public static JSONArray toJSONArray(ResultSet res) {
        try {
            JSONArray jsonArray = new JSONArray();

            while (res.next()) {

                int total_rows = res.getMetaData().getColumnCount();
                JSONObject json_object = new JSONObject();

                for (int i = 0; i < total_rows; i++) {

                    json_object.put(res.getMetaData().getColumnLabel(i + 1).toLowerCase(), res.getObject(i + 1));
                }

                jsonArray.put(json_object);
            }
            return jsonArray;
        } catch (SQLException ex) {
            System.out.println("generic.ResultSetToJSON.toJSON()");
            Logger.getLogger(ResultSetToJSON.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static JSONObject toJSON(ResultSet res) {
        try {

            if (res.next()) {

                int total_rows = res.getMetaData().getColumnCount();
                JSONObject json_object = new JSONObject();

                for (int i = 0; i < total_rows; i++) {

                    json_object.put(res.getMetaData().getColumnLabel(i + 1).toLowerCase(), res.getObject(i + 1));
                }

                return json_object;
            }
        } catch (SQLException ex) {
            System.out.println("generic.ResultSetToJSON.toJSONRow()");
            Logger.getLogger(ResultSetToJSON.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
