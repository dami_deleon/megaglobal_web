/*
 * Copyright 2017 damian.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package modelo;

import generic.ResultSetToJSON;
import generic.conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author damian
 */
public class mUsuario {

    private String usu_user, usu_clave, usu_mail, token_ip;
    private int usu_cod, per_cod, emp_cod;
    private boolean usu_estado;
    private JSONObject token_create_by;
    conexion cone = new conexion();

    public String getToken_ip() {
        return token_ip;
    }

    public void setToken_ip(String token_ip) {
        this.token_ip = token_ip;
    }

    public JSONObject getToken_create_by() {
        return token_create_by;
    }

    public void setToken_create_by(JSONObject token_create_by) {
        this.token_create_by = token_create_by;
    }

    public String getUsu_user() {
        return usu_user;
    }

    public void setUsu_user(String usu_user) {
        this.usu_user = usu_user;
    }

    public String getUsu_clave() {
        return usu_clave;
    }

    public void setUsu_clave(String usu_clave) {
        this.usu_clave = usu_clave;
    }

    public int getUsu_cod() {
        return usu_cod;
    }

    public void setUsu_cod(int usu_cod) {
        this.usu_cod = usu_cod;
    }

    public int getPer_cod() {
        return per_cod;
    }

    public void setPer_cod(int per_cod) {
        this.per_cod = per_cod;
    }

    public int getEmp_cod() {
        return emp_cod;
    }

    public void setEmp_cod(int emp_cod) {
        this.emp_cod = emp_cod;
    }

    public boolean isUsu_estado() {
        return usu_estado;
    }

    public void setUsu_estado(boolean usu_estado) {
        this.usu_estado = usu_estado;
    }

    public String getUsu_mail() {
        return usu_mail;
    }

    public void setUsu_mail(String usu_mail) {
        this.usu_mail = usu_mail;
    }

    public JSONObject insert_usuario() {
        JSONObject jo = new JSONObject();
        /*
        <_usu_user integer>,
        <_emp_cod integer>,
        <_per_cod integer>,
        <_usu_mail character varying>
        <_token_create_by json>
        <_token_ip character varying>
         */
        String sql_insert = "SELECT public.insert_user_token(?, ?, ?, ?, ?::json, ?) as token";
        try {
            PreparedStatement ps_u = cone.conectar().prepareStatement(sql_insert);
            ps_u.setString(1, getUsu_user());
            ps_u.setInt(2, getEmp_cod());
            ps_u.setInt(3, getPer_cod());
            ps_u.setString(4, getUsu_mail());
            ps_u.setString(5, getToken_create_by().toString());
            ps_u.setString(6, getToken_ip());

            return ResultSetToJSON.toJSON(ps_u.executeQuery());

        } catch (SQLException ex) {
            jo.put("message", ex.getMessage());
            return jo.put("error", true);

        }

    }
    
    public int save_user (){
        try {
            String sql = "UPDATE public.usuario set usu_clave=MD5(?), usu_estado = true where usu_cod = ?;";
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            ps.setString(1, getUsu_clave());
            ps.setInt(2, getUsu_cod());
            return ps.executeUpdate();
        } catch (SQLException ex) {
            return 0;
        }
        
    }

    public JSONObject verificar_token(String token) {
        JSONObject json = new JSONObject();
        String SQL = "SELECT t.token_cod,t.token_valid_datetime > CURRENT_TIMESTAMP as valid, t.token_last_access, u.usu_mail, u.usu_user, u.usu_cod\n"
                + "FROM public.tokens t, public.usuario u\n"
                + "WHERE t.usu_cod = u.usu_cod AND\n"
                + "token_cod = ?;";

        PreparedStatement ps;
        try {
            ps = cone.conectar().prepareStatement(SQL);
            ps.setString(1, token);
            return json = ResultSetToJSON.toJSON(ps.executeQuery());

        } catch (SQLException ex) {
            json.put("message", ex.getMessage());
            return json.put("error", true);
        }

    }

    public JSONObject listar_usuarios() {
        JSONArray array = new JSONArray();
        JSONObject json = new JSONObject();
        String SQL = "SELECT\n"
                + "e.emp_nombre, e.emp_apellido, e.emp_ci, p.per_descripcion, u.usu_cod, u.hora_sesion, u.usu_user, u.usu_estado, u.emp_cod, u.per_cod\n"
                + "FROM\n"
                + "public.usuario u, public.empleado e, public.perfil p\n"
                + "where\n"
                + "u.usu_estado = true and\n"
                + "e.emp_cod = u.emp_cod and\n"
                + "p.per_cod = u.per_cod;";
        try {
            ResultSet rs = cone.conectar().prepareStatement(SQL).executeQuery();
            array = generic.ResultSetToJSON.toJSONArray(rs);
            return json.put("usuarios", array);
        } catch (SQLException | JSONException e) {
            JSONObject msg = new JSONObject();
            msg.put("sql", SQL);
            msg.put("error", e.getMessage());
            return msg;
        }

    }

}
