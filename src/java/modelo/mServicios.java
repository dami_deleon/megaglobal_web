/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import generic.ResultSetToJSON;
import generic.conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.json.JSONObject;

/**
 * ser_cod integer NOT NULL, ser_descripcion character varying(100) NOT NULL,
 * ser_costo_mensual integer NOT NULL, ser_costo_unitario integer NOT NULL,
 * med_cod integer NOT NULL,
 *
 * @author damian
 */
public class mServicios {

    private Integer ser_cod, ser_costo_mensual, ser_costo_unitario, med_cod;
    private String ser_descripcion;
    conexion cone = new conexion();

    public Integer getSer_cod() {
        return ser_cod;
    }

    public void setSer_cod(Integer ser_cod) {
        this.ser_cod = ser_cod;
    }

    public Integer getSer_costo_mensual() {
        return ser_costo_mensual;
    }

    public void setSer_costo_mensual(Integer ser_costo_mensual) {
        this.ser_costo_mensual = ser_costo_mensual;
    }

    public Integer getSer_costo_unitario() {
        return ser_costo_unitario;
    }

    public void setSer_costo_unitario(Integer ser_costo_unitario) {
        this.ser_costo_unitario = ser_costo_unitario;
    }

    public Integer getMed_cod() {
        return med_cod;
    }

    public void setMed_cod(Integer med_cod) {
        this.med_cod = med_cod;
    }

    public String getSer_descripcion() {
        return ser_descripcion;
    }

    public void setSer_descripcion(String ser_descripcion) {
        this.ser_descripcion = ser_descripcion;
    }

    public int insertar() {

        String sql = "INSERT INTO public.servicios( ser_cod, ser_descripcion, ser_costo_mensual, ser_costo_unitario, med_cod)\n"
                + "VALUES ((select coalesce(max(ser_cod),0)+1 from servicios), ?, ?, ?, ?);";

        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            ps.setString(1, getSer_descripcion());
            ps.setInt(2, getSer_costo_mensual());
            ps.setInt(3, getSer_costo_unitario());
            ps.setInt(4, getMed_cod());
            return ps.executeUpdate();
        } catch (Exception ex) {
            System.err.println("Error al insertar \n" + ex);
            return 0;
        }

    }

    public int modificar() {
        String sql = "UPDATE public.servicios SET ser_descripcion=?, ser_costo_mensual=?, ser_costo_unitario=?, med_cod=? WHERE ser_cod=?;";

        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            ps.setString(1, getSer_descripcion());
            ps.setInt(2, getSer_costo_mensual());
            ps.setInt(3, getSer_costo_unitario());
            ps.setInt(4, getMed_cod());
            ps.setInt(5, getSer_cod());

            return ps.executeUpdate();

        } catch (Exception ex) {
            System.err.println("Error modificar \n" + ex);
            return 0;
        }
    }

    public int eliminar() {
        String sql = "DELETE FROM public.servicios WHERE ser_cod=?;";
        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            ps.setInt(1, getSer_cod());
            return ps.executeUpdate();
        } catch (Exception ex) {
            System.err.println("Error modificar \n" + ex);
            return 0;
        }
    }

    public JSONObject servicios() {
        JSONObject json = new JSONObject();

        String sql = "SELECT servicios.ser_cod, servicios.ser_descripcion, \n"
                + "servicios.ser_costo_mensual, servicios.ser_costo_unitario, \n"
                + "servicios.med_cod, medio.med_descripcion\n"
                + "FROM public.servicios, public.medio\n"
                + "WHERE servicios.med_cod = medio.med_cod;";

        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            ResultSet res = ps.executeQuery();

            return json.put("data", ResultSetToJSON.toJSONArray(res));

        } catch (Exception ex) {
            System.err.println("Error servicios \n" + ex.getMessage());
            return null;
        }

    }

    public JSONObject verifica_ruc() {
        JSONObject json = new JSONObject();
        String sql = "SELECT coalesce((select false from public.cliente where cliente.cli_ci_ruc = ?), true) as ok;";
        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);

            ResultSet res = ps.executeQuery();
            return ResultSetToJSON.toJSONArray(res).getJSONObject(0);
        } catch (Exception ex) {
            System.err.println("Error verificar CI / RUC \n" + ex);
            return null;
        }
    }

    public JSONObject medios() {

        JSONObject json = new JSONObject();

        String sql = "SELECT medio.med_cod, medio.med_descripcion FROM public.medio;";

        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            ResultSet res = ps.executeQuery();

            return json.put("medios", ResultSetToJSON.toJSONArray(res));

        } catch (Exception ex) {
            System.err.println("Error ciudades \n" + ex.getMessage());
            return null;
        }

    }
}
