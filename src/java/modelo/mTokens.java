/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.PreparedStatement;
import org.json.JSONObject;
import generic.conexion;
import generic.ResultSetToJSON;
import java.sql.ResultSet;

/**
 *
 * @author Damián
 */
public class mTokens {

    /**
     * token_id integer NOT NULL, token_cod character varying(30) COLLATE
     * "default".pg_catalog NOT NULL, token_valid_date date NOT NULL,
     * token_ceated_by json, token_last_access timestamp without time zone,
     * token_ip_acceded character varying(60) COLLATE "default".pg_catalog,
     */
    private Integer token_id;
    private String token_cod, token_valid_date, token_ip_acceded;
    private JSONObject token_ceated_by;
    conexion cone = new conexion();

    public JSONObject refreshCargos() {
        JSONObject json = new JSONObject();

        String sql = "SELECT car_cod, car_descri FROM cargo order by car_descri;";
        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            ResultSet res = ps.executeQuery();

            return json.put("data", ResultSetToJSON.toJSON(res));

        } catch (Exception ex) {
            System.err.println("Error refreshCargos \n" + ex.getMessage());
            return null;
        }

    }

    public int insertar() {
        String sql = "INSERT INTO public.tokens(\n"
                + "token_id, token_cod, token_valid_datetime, token_ceated_by, token_last_access, token_ip_acceded, usu_cod)\n"
                + "VALUES ((select coalesce(max(token_id), 0)+1 from public.tokens), ?, (current_timestamp + interval '30 minutes'), ?, current_timestamp, ?, ?);";

        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            //ps.setString(1, getCar_descri());
            return ps.executeUpdate();
        } catch (Exception ex) {
            System.err.println("Error insertar \n" + ex);
            return 0;
        }
    }

    public int modificar() {
        String sql = "UPDATE cargo SET car_descri=? WHERE car_cod=?;";
        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            //ps.setString(1, getCar_descri());
            //ps.setInt(2, getCar_cod());
            return ps.executeUpdate();
        } catch (Exception ex) {
            System.err.println("Error modificar \n" + ex);
            return 0;
        }
    }

    public int eliminar() {
        String sql = "DELETE FROM cargo WHERE car_cod=?;";
        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            //ps.setInt(1, getCar_cod());
            return ps.executeUpdate();
        } catch (Exception ex) {
            System.err.println("Error eliminar \n" + ex);
            return 0;
        }

    }

}
