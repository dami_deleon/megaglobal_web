/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import generic.ResultSetToJSON;
import generic.conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.json.JSONObject;

/**
 *
 * @author damian
 */
public class mClientes {

    private int cli_cod, ciu_cod;
    private String cli_nombre, cli_ci_ruc, cli_email, cli_direccion, cli_telefono;
    conexion cone = new conexion();

    public int getCli_cod() {
        return cli_cod;
    }

    public void setCli_cod(int cli_cod) {
        this.cli_cod = cli_cod;
    }

    public int getCiu_cod() {
        return ciu_cod;
    }

    public void setCiu_cod(int ciu_cod) {
        this.ciu_cod = ciu_cod;
    }

    public String getCli_nombre() {
        return cli_nombre;
    }

    public void setCli_nombre(String cli_nombre) {
        this.cli_nombre = cli_nombre;
    }

    public String getCli_ci_ruc() {
        return cli_ci_ruc;
    }

    public void setCli_ci_ruc(String cli_ci_ruc) {
        this.cli_ci_ruc = cli_ci_ruc;
    }

    public String getCli_email() {
        return cli_email;
    }

    public void setCli_email(String cli_email) {
        this.cli_email = cli_email;
    }

    public String getCli_direccion() {
        return cli_direccion;
    }

    public void setCli_direccion(String cli_direccion) {
        this.cli_direccion = cli_direccion;
    }

    public String getCli_telefono() {
        return cli_telefono;
    }

    public void setCli_telefono(String cli_telefono) {
        this.cli_telefono = cli_telefono;
    }

    public int insertar() {

        String sql = "INSERT INTO public.cliente(cli_cod, cli_nombre, cli_ci_ruc, cli_email, cli_direccion, cli_telefono, ciu_cod)\n"
                + "VALUES ((select coalesce (max(cli_cod),0)+1 from cliente), ?, ?, ?, ?, ?, ?);";

        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            ps.setString(1, getCli_nombre());
            ps.setString(2, getCli_ci_ruc());
            ps.setString(3, getCli_email());
            ps.setString(4, getCli_direccion());
            ps.setString(5, getCli_telefono());
            ps.setInt(6, getCiu_cod());
            return ps.executeUpdate();
        } catch (Exception ex) {
            System.err.println("Error insertar \n" + ex);
            return 0;
        }

    }

    public int modificar() {
        String sql = "UPDATE public.cliente\n"
                + "SET cli_nombre=?, cli_ci_ruc=?, cli_email=?, cli_direccion=?, cli_telefono=?,  ciu_cod=?\n"
                + "WHERE cli_cod=?;";

        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            ps.setString(1, getCli_nombre());
            ps.setString(2, getCli_ci_ruc());
            ps.setString(3, getCli_email());
            ps.setString(4, getCli_direccion());
            ps.setString(5, getCli_telefono());
            ps.setInt(6, getCiu_cod());
            ps.setInt(7, getCli_cod());
            return ps.executeUpdate();
        } catch (Exception ex) {
            System.err.println("Error modificar \n" + ex);
            return 0;
        }
    }

    public int eliminar() {

        String sql = "DELETE FROM public.cliente\n"
                + "WHERE cli_cod=?;";
        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            ps.setInt(1, getCli_cod());
            return ps.executeUpdate();
        } catch (Exception ex) {
            System.err.println("Error modificar \n" + ex);
            return 0;
        }
    }

    public JSONObject clientes() {
        JSONObject json = new JSONObject();

        String sql = "SELECT \n"
                + "cliente.cli_cod, \n"
                + "cliente.cli_nombre, \n"
                + "cliente.cli_ci_ruc, \n"
                + "cliente.cli_email, \n"
                + "cliente.cli_direccion, \n"
                + "cliente.cli_sector, \n"
                + "cliente.ciu_cod, \n"
                + "cliente.cli_telefono, \n"
                + "ciudad.ciu_cod, \n"
                + "ciudad.ciu_descri\n"
                + "FROM \n"
                + "public.cliente, \n"
                + "public.ciudad\n"
                + "WHERE \n"
                + "cliente.ciu_cod = ciudad.ciu_cod\n"
                + "ORDER BY cliente.cli_cod;";

        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            ResultSet res = ps.executeQuery();

            return json.put("data", ResultSetToJSON.toJSONArray(res));

        } catch (Exception ex) {
            System.err.println("Error clientes \n" + ex.getMessage());
            return null;
        }

    }
    
    public JSONObject verifica_ruc(){
        JSONObject json = new JSONObject();
        String sql = "SELECT coalesce((select false from public.cliente where cliente.cli_ci_ruc = ?), true) as ok;";
        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            ps.setString(1, getCli_ci_ruc());
            ResultSet res = ps.executeQuery();
            return ResultSetToJSON.toJSONArray(res).getJSONObject(0);
        } catch (Exception ex) {
            System.err.println("Error verificar CI / RUC \n" + ex);
            return null;
        }
    }

    public JSONObject ciudades() {

        JSONObject json = new JSONObject();

        String sql = "SELECT ciu_cod, ciu_descri\n"
                + "FROM public.ciudad order by ciu_cod;";

        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            ResultSet res = ps.executeQuery();

            return json.put("ciudad", ResultSetToJSON.toJSONArray(res));

        } catch (Exception ex) {
            System.err.println("Error ciudades \n" + ex.getMessage());
            return null;
        }

    }
}
