/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import generic.conexion;
import generic.ResultSetToJSON;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Damián
 */
public class mPropuestas {

    conexion cone = new conexion();
    Connection coneccion = cone.conectar();

    private Integer pro_cod, emp_cod, cli_cod;
    private String pro_date, token;
    private Boolean pro_estado;
    private JSONArray detalle;

    public mPropuestas() {

    }

    public JSONArray getDetalle() {
        return detalle;
    }

    public void setDetalle(JSONArray detalle) {
        this.detalle = detalle;
    }

    public Integer getPro_cod() {
        return pro_cod;
    }

    public void setPro_cod(Integer pro_cod) {
        this.pro_cod = pro_cod;
    }

    public Integer getEmp_cod() {
        return emp_cod;
    }

    public void setEmp_cod(Integer emp_cod) {
        this.emp_cod = emp_cod;
    }

    public Integer getCli_cod() {
        return cli_cod;
    }

    public void setCli_cod(Integer cli_cod) {
        this.cli_cod = cli_cod;
    }

    public Boolean getPro_estado() {
        return pro_estado;
    }

    public void setPro_estado(Boolean pro_estado) {
        this.pro_estado = pro_estado;
    }

    public String getPro_date() {
        return pro_date;
    }

    public void setPro_date(String pro_date) {
        this.pro_date = pro_date;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public JSONObject nuevaPropuesta() throws SQLException {

        PreparedStatement ps_c = null;
        PreparedStatement ps_d = null;
        setPro_cod(pro_cod_genera());
        JSONObject jo = new JSONObject();

        int det[];
        int cab = 0;

        String c = "INSERT INTO public.propuesta(pro_cod, emp_cod, cli_cod, pro_estado, pro_fecha) VALUES (?, ?, ?, true, current_date);";
        String d = "INSERT INTO public.detalle_propuesta(ser_cod, pro_cod, det_estado, det_cantidad, det_monto) VALUES (?, ?, ?, ?, ?);";

        try {
            coneccion.setAutoCommit(false);

            ps_c = coneccion.prepareStatement(c);
            ps_d = coneccion.prepareStatement(d);

            //setea los datos de la cabecera
            ps_c.setInt(1, getPro_cod());
            ps_c.setInt(2, getEmp_cod());
            ps_c.setInt(3, getCli_cod());
            //inserta cabecera
            cab = ps_c.executeUpdate();
            // Tamaño del arreglo del detalle
            int L = getDetalle().length();
            for (int i = 0; i < L; i++) {
                ps_d.setInt(1, getDetalle().getJSONObject(i).getInt("ser_cod"));
                ps_d.setInt(2, getPro_cod());
                ps_d.setBoolean(3, true);
                ps_d.setInt(4, getDetalle().getJSONObject(i).getInt("det_cantidad"));
                ps_d.setInt(5, getDetalle().getJSONObject(i).getInt("det_monto"));
                ps_d.addBatch();
            }
            //inserta cabecera
            det = ps_d.executeBatch();

            if (det.length == L) {
                coneccion.commit();

                jo.put("c", cab);
                jo.put("d", det);
            } else {
                coneccion.rollback();
                jo.put("error", "No se ha podido insertar los datos");
            }

        } catch (SQLException ex) {
            try {

                System.err.println("Se ha producido un error en guardar los datos\n" + ex.getLocalizedMessage());
                cone.conectar().rollback();
                jo.put("error", ex.getMessage());
            } catch (SQLException ex1) {
                System.err.println("Se ha producido un error en realizar rollback\n" + ex1.getLocalizedMessage());
                jo.put("error", ex.getMessage());
            }
        } finally {

            if (ps_d != null) {
                ps_d.close();
            }

            if (ps_c != null) {
                ps_c.close();
            }

            coneccion.setAutoCommit(true);
        }
        return jo;
    }

    public JSONObject servicios() {
        String sql = "SELECT ser_cod, ser_descripcion, ser_costo_mensual, ser_costo_unitario, med_cod FROM servicios;";
        JSONObject json = new JSONObject();

        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            ResultSet res = ps.executeQuery();

            return json.put("data", ResultSetToJSON.toJSONArray(res));

        } catch (Exception ex) {
            System.err.println("Error servicios \n" + ex.getMessage());
            return null;
        }
    }

    public JSONObject clientes() {
        String sql = "SELECT cliente.cli_nombre || ' ( ' || cliente.cli_ci_ruc || ' )' as cli_razon_social,  cliente.cli_cod FROM public.cliente order by cli_razon_social;";
        JSONObject json = new JSONObject();

        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            ResultSet res = ps.executeQuery();

            return json.put("clientes", ResultSetToJSON.toJSONArray(res));

        } catch (Exception ex) {
            System.err.println("Error servicios \n" + ex.getMessage());
            return null;
        }
    }

    private Integer pro_cod_genera() {
        try {
            PreparedStatement ps = cone.conectar().prepareStatement("SELECT coalesce(max(pro_cod), 0) + 1 FROM public.propuesta;");
            ResultSet r = ps.executeQuery();

            if (r.next()) {
                return r.getInt(1);
            }

            return 0;
        } catch (SQLException ex) {
            System.out.println("ERROR modelo.mPropuestas.pro_cod_genera() :\n" + ex.getMessage());
        }
        return 0;
    }

    public JSONObject propuestas() {
        JSONObject json = new JSONObject();
        try {
            String sql = "SELECT to_char(p.pro_fecha, 'DD/MM/YYYY') as pro_fecha, p.pro_cod, p.pro_estado, e.emp_nombre ||' '||e.emp_apellido as emp_nombreapellido, c.cli_nombre\n"
                    + "FROM public.propuesta p, public.empleado e, public.cliente c\n"
                    + "WHERE e.emp_cod = p.emp_cod AND c.cli_cod = p.cli_cod\n"
                    + "ORDER BY p.pro_cod DESC;";
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            ResultSet res = ps.executeQuery();
            return json.put("propuestas", ResultSetToJSON.toJSONArray(res));
        } catch (SQLException e) {
            System.err.println("ERROR modelo.mPropuestas.propuestas() \n" + e.getMessage());
        }

        return null;
    }

    public JSONObject propuestaByCod() {
        JSONObject JSONPropuesta = new JSONObject();

        String sqlPropuesta = "SELECT empleado.emp_nombre, empleado.emp_apellido, \n"
                + "empleado.emp_ci, cliente.cli_nombre, cliente.cli_ci_ruc,\n"
                + "to_char(propuesta.pro_fecha, 'dd/MM/YYYY') as pro_fecha, \n"
                + "propuesta.pro_estado FROM  public.propuesta,  public.cliente, \n"
                + "public.empleado WHERE  cliente.cli_cod = propuesta.cli_cod AND\n"
                + "empleado.emp_cod = propuesta.emp_cod AND propuesta.pro_cod = ?;";
        String sqlServicios = "SELECT detalle_propuesta.det_monto, \n"
                + "detalle_propuesta.pro_cod, servicios.med_cod, servicios.ser_descripcion, \n"
                + "medio.med_descripcion, detalle_propuesta.det_cantidad\n"
                + "FROM public.detalle_propuesta, public.servicios, \n"
                + "public.medio WHERE  detalle_propuesta.ser_cod = servicios.ser_cod AND \n"
                + "medio.med_cod = servicios.med_cod AND detalle_propuesta.pro_cod = ?  "
                + "ORDER BY servicios.med_cod";
        try {
            PreparedStatement ps_1 = cone.prepararSQL(sqlPropuesta);
            PreparedStatement ps_2 = cone.prepararSQL(sqlServicios);
            ps_1.setInt(1, getPro_cod());
            ps_2.setInt(1, getPro_cod());
            JSONPropuesta = ResultSetToJSON.toJSON(ps_1.executeQuery());
            JSONPropuesta.put("detalle", ResultSetToJSON.toJSONArray(ps_2.executeQuery()));
        } catch (SQLException | JSONException e) {
            JSONPropuesta.put("error", e.getMessage());
        }

        return JSONPropuesta;
    }
    
    public void createToken(){
        String sql = "";
    }

}
