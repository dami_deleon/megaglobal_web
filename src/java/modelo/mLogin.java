/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import generic.conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.json.JSONObject;

/**
 *
 * @author Damián
 */
public class mLogin {

    /*
    usu_cod integer primary key,
	usu_user varchar(30) not null,
	usu_password varchar(30) not null,
	usu_ip varchar(15),
	usu_session varchar(30)
     */
    private int usu_cod;
    private String usu_user;
    private String usu_password;
    private String usu_ip;
    private String usu_session;
    private final conexion cone;

    public mLogin() {
        cone = new conexion();

    }

    public int getUsu_cod() {
        return usu_cod;
    }

    public void setUsu_cod(int usu_cod) {
        this.usu_cod = usu_cod;
    }

    public String getUsu_user() {
        return usu_user;
    }

    public void setUsu_user(String usu_user) {
        this.usu_user = usu_user;
    }

    public String getUsu_password() {
        return usu_password;
    }

    public void setUsu_password(String usu_password) {
        this.usu_password = usu_password;
    }

    public String getUsu_ip() {
        return usu_ip;
    }

    public void setUsu_ip(String usu_ip) {
        this.usu_ip = usu_ip;
    }

    public String getUsu_session() {
        return usu_session;
    }

    public void setUsu_session(String usu_session) {
        this.usu_session = usu_session;
    }

    public void insertarData() throws SQLException {
        String sql = "UPDATE public.usuario\n"
                + "SET id_sesion=?, hora_sesion=current_timestamp\n"
                + "WHERE usu_user=?;";
        PreparedStatement ps = cone.conectar().prepareStatement(sql);
        ps.setString(1, getUsu_session());
        ps.setString(2, getUsu_user());

        ps.executeUpdate();
    }

    public boolean consultar() {

        try {
            String sql = "SELECT usu_cod, id_sesion, hora_sesion, usu_user, usu_estado, emp_cod, \n"
                    + "per_cod, usu_clave FROM usuario "
                    + "where usu_user = ? and usu_clave = md5(?);";

            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            ps.setString(1, getUsu_user());
            ps.setString(2, getUsu_password());
            ResultSet res = ps.executeQuery();

            return res.next();
        } catch (SQLException ex) {
            System.err.println("Error" + ex.getMessage());
        }
        return false;
    }

    public JSONObject usuario() throws SQLException, Exception {

        String sql = "SELECT usu_cod, id_sesion, hora_sesion, usu_user, usu_estado, emp_cod, \n"
                + "per_cod FROM usuario "
                + "where id_sesion = ?";
        PreparedStatement ps = cone.conectar().prepareStatement(sql);
        ps.setString(1, getUsu_session());
        ResultSet res = ps.executeQuery();

        return generic.ResultSetToJSON.toJSONArray(res).getJSONObject(0);
    }

    public Integer usuario(String session) throws SQLException, Exception {

        String sql = "SELECT usu_cod, id_sesion, hora_sesion, usu_user, usu_estado, emp_cod, \n"
                + "per_cod FROM usuario "
                + "where id_sesion = ?";
        PreparedStatement ps = cone.conectar().prepareStatement(sql);
        ps.setString(1, session);
        ResultSet res = ps.executeQuery();

        return generic.ResultSetToJSON.toJSONArray(res).getJSONObject(0).getInt("emp_cod");
    }

}
