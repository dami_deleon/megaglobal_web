/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.PreparedStatement;
import org.json.JSONObject;
import generic.conexion;
import generic.ResultSetToJSON;
import java.sql.ResultSet;

/**
 *
 * @author Damián
 */
public class mCiudades {

    private Integer ciu_cod;
    private String ciu_descri;
    conexion cone = new conexion();

public Integer getCiu_cod() {
        return ciu_cod;
    }

    public void setCiu_cod(Integer ciu_cod) {
        this.ciu_cod = ciu_cod;
    }

    public String getCiu_descri() {
        return ciu_descri;
    }

    public void setCiu_descri(String ciu_descri) {
        this.ciu_descri = ciu_descri;
    }

    public JSONObject ciudades() {
        JSONObject json = new JSONObject();

        String sql = "SELECT ciu_cod, ciu_descri FROM public.ciudad order by ciu_cod;";
        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            ResultSet res = ps.executeQuery();

            return json.put("data", ResultSetToJSON.toJSONArray(res));

        } catch (Exception ex) {
            System.err.println("Error ciudades \n" + ex.getMessage());
            return null;
        }

    }

    public int insertar() {
        String sql = "INSERT INTO ciudad(ciu_cod, ciu_descri) VALUES ((select coalesce (max(ciu_cod),0)+1 from ciudad), ?);";

        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            ps.setString(1, getCiu_descri());
            return ps.executeUpdate();
        } catch (Exception ex) {
            System.err.println("Error insertar \n" + ex);
            return 0;
        }
    }

    public int modificar() {
        String sql = "UPDATE ciudad SET ciu_descri=? WHERE ciu_cod=?;";
        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            ps.setString(1, getCiu_descri());
            ps.setInt(2, getCiu_cod());
            return ps.executeUpdate();
        } catch (Exception ex) {
            System.err.println("Error modificar \n" + ex);
            return 0;
        }
    }

    public int eliminar() {
        String sql = "DELETE FROM ciudad WHERE ciu_cod=?;";
        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            ps.setInt(1, getCiu_cod());
            return ps.executeUpdate();
        } catch (Exception ex) {
            System.err.println("Error eliminar \n" + ex);
            return 0;
        }

    }

}
