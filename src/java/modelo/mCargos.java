/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.PreparedStatement;
import org.json.JSONObject;
import generic.conexion;
import generic.ResultSetToJSON;
import java.sql.ResultSet;

/**
 *
 * @author Damián
 */
public class mCargos {

    private Integer car_cod;
    private String car_descri;
    conexion cone = new conexion();

    public int getCar_cod() {
        return car_cod;
    }

    public void setCar_cod(int car_cod) {
        this.car_cod = car_cod;
    }

    public String getCar_descri() {
        return car_descri;
    }

    public void setCar_descri(String car_descri) {
        this.car_descri = car_descri;
    }

    public JSONObject refreshCargos() {
        JSONObject json = new JSONObject();

        String sql = "SELECT car_cod, car_descri FROM cargo order by car_descri;";
        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            ResultSet res = ps.executeQuery();

            return json.put("data", ResultSetToJSON.toJSONArray(res));

        } catch (Exception ex) {
            System.err.println("Error refreshCargos \n" + ex.getMessage());
            return null;
        }

    }

    public int insertar() {
        String sql = "INSERT INTO cargo(car_cod, car_descri) VALUES ((select coalesce (max(car_cod),0)+1 from cargo), ?);";

        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            ps.setString(1, getCar_descri());
            return ps.executeUpdate();
        } catch (Exception ex) {
            System.err.println("Error insertar \n" + ex);
            return 0;
        }
    }

    public int modificar() {
        String sql = "UPDATE cargo SET car_descri=? WHERE car_cod=?;";
        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            ps.setString(1, getCar_descri());
            ps.setInt(2, getCar_cod());
            return ps.executeUpdate();
        } catch (Exception ex) {
            System.err.println("Error modificar \n" + ex);
            return 0;
        }
    }

    public int eliminar() {
        String sql = "DELETE FROM cargo WHERE car_cod=?;";
        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            ps.setInt(1, getCar_cod());
            return ps.executeUpdate();
        } catch (Exception ex) {
            System.err.println("Error eliminar \n" + ex);
            return 0;
        }

    }

}
