/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.PreparedStatement;
import org.json.JSONObject;
import generic.conexion;
import generic.ResultSetToJSON;
import java.sql.ResultSet;

/**
 *
 * @author Damián
 */
public class mPerfilesUsuario {

    
    conexion cone = new conexion();

    public JSONObject listarPerfiles() {
        JSONObject json = new JSONObject();

        String sql = "SELECT * FROM perfil order by per_cod;";
        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            ResultSet res = ps.executeQuery();

            return json.put("perfiles", ResultSetToJSON.toJSONArray(res));

        } catch (Exception ex) {
            json.put("error", ex.getCause() + "\n" + ex.getMessage());
            return null;
        }

    }

    public int insertar() {
        String sql = "INSERT INTO cargo(car_cod, car_descri) VALUES ((select coalesce (max(car_cod),0)+1 from cargo), ?);";

        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            //ps.setString(1, getCar_descri());
            return ps.executeUpdate();
        } catch (Exception ex) {
            System.err.println("Error insertar \n" + ex);
            return 0;
        }
    }

    public int modificar() {
        String sql = "UPDATE cargo SET car_descri=? WHERE car_cod=?;";
        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            //ps.setString(1, getCar_descri());
            //ps.setInt(2, getCar_cod());
            return ps.executeUpdate();
        } catch (Exception ex) {
            System.err.println("Error modificar \n" + ex);
            return 0;
        }
    }

    public int eliminar() {
        String sql = "DELETE FROM cargo WHERE car_cod=?;";
        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            //ps.setInt(1, getCar_cod());
            return ps.executeUpdate();
        } catch (Exception ex) {
            System.err.println("Error eliminar \n" + ex);
            return 0;
        }

    }

}
