/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.PreparedStatement;
import org.json.JSONObject;
import generic.conexion;
import generic.ResultSetToJSON;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.json.JSONException;

/**
 *
 * @author Damián
 */
public class mEmpleados {

    conexion cone = new conexion();

    public JSONObject listarEmpleados() {
        JSONObject json = new JSONObject();

        String sql = "SELECT e.emp_cod, e.emp_nombre, e.emp_apellido,\n"
                + "e.emp_ci, e.emp_direccion, to_char(e.emp_fecha_nacimiento, 'dd/mm/yyyy') as emp_fecha_nacimiento, c.car_descri\n"
                + "FROM public.empleado e, public.cargo c\n"
                + "WHERE c.car_cod = e.car_cod and\n"
                + "NOT EXISTS (select emp_cod from public.usuario u where e.emp_cod=u.emp_cod)\n"
                + "ORDER BY e.emp_apellido;";
        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            ResultSet res = ps.executeQuery();
            return json.put("empleados", ResultSetToJSON.toJSONArray(res));
        } catch (SQLException | JSONException ex) {
            json.put("error", ex.getCause() + "\n" + ex.getMessage());
            return json;
        }

    }

    public int insertar() {
        String sql = "INSERT INTO cargo(car_cod, car_descri) VALUES ((select coalesce (max(car_cod),0)+1 from cargo), ?);";

        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            //ps.setString(1, getCar_descri());
            return ps.executeUpdate();
        } catch (Exception ex) {
            System.err.println("Error insertar \n" + ex);
            return 0;
        }
    }

    public int modificar() {
        String sql = "UPDATE cargo SET car_descri=? WHERE car_cod=?;";
        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            //ps.setString(1, getCar_descri());
            //ps.setInt(2, getCar_cod());
            return ps.executeUpdate();
        } catch (Exception ex) {
            System.err.println("Error modificar \n" + ex);
            return 0;
        }
    }

    public int eliminar() {
        String sql = "DELETE FROM cargo WHERE car_cod=?;";
        try {
            PreparedStatement ps = cone.conectar().prepareStatement(sql);
            //ps.setInt(1, getCar_cod());
            return ps.executeUpdate();
        } catch (Exception ex) {
            System.err.println("Error eliminar \n" + ex);
            return 0;
        }

    }

}
