/*
 * Copyright 2017 damian.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package control;

import generic.RandomString;
import generic.Mailer;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.mEmpleados;
import modelo.mLogin;
import modelo.mPerfilesUsuario;
import modelo.mUsuario;
import org.json.JSONObject;

/**
 *
 * @author damian
 */
@WebServlet(name = "usuario", urlPatterns = {"/usuario"})
public class usuario extends HttpServlet {

    mUsuario usu = new mUsuario();
    mEmpleados me = new mEmpleados();
    mPerfilesUsuario mp = new mPerfilesUsuario();
    mLogin ml = new mLogin();
    RandomString rs = new RandomString();
    Mailer mailSend = new Mailer();

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect(request.getContextPath() + "/#/usuario");
        //processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        JSONObject json = new JSONObject();
        response.setContentType("application/json;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String o = "err";
            String s = request.getSession().getId();
            if (!s.isEmpty()) {
                o = request.getParameter("o");
            }
            
            String token = request.getParameter("token");
            String usu_user = request.getParameter("usu_user");
            String emp_cod = request.getParameter("emp_cod");
            String per_cod = request.getParameter("per_cod");
            String usu_mail = request.getParameter("usu_mail");
            String token_ip = request.getRemoteAddr();
            String usu_clave = request.getParameter("usu_clave");
            String usu_cod = request.getParameter("usu_cod");
            
            switch (o) {
                case "listar":
                    out.print(usu.listar_usuarios());
                    break;
                case "emp":
                    out.print(me.listarEmpleados());
                    break;
                case "sp":
                    usu.setUsu_cod(Integer.parseInt(usu_cod));
                    usu.setUsu_clave(usu_clave);
                    
                    json.put("result", usu.save_user());
                    out.print(json);
                    break;
                case "per":
                    out.print(mp.listarPerfiles());
                    break;
                case "save":
                    JSONObject mail = new JSONObject();
                    ml.setUsu_session(request.getSession().getId());
                    usu.setToken_create_by(ml.usuario());
                    mail.put("usu_id", ml.usuario());
                    usu.setUsu_user(usu_user);
                    usu.setEmp_cod(Integer.parseInt(emp_cod));
                    usu.setPer_cod(Integer.parseInt(per_cod));
                    usu.setUsu_mail(usu_mail);
                    usu.setToken_ip(token_ip);
                    JSONObject res = usu.insert_usuario(); //inserta usuario
                    
                    out.print(mailSend.establecer_cuenta(usu_mail, res.getString("token")));

                    break;
                case "vt":
                    json.put("token", token);
                    out.print(usu.verificar_token(token));
                    break;

            }

        } catch (Exception e) {
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
