/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.mCargos;
import modelo.mCiudades;
import org.json.JSONTokener;

/**
 *
 * @author Damián
 */
@WebServlet(name = "ciudad", urlPatterns = {"/ciudad"})
public class ciudad extends HttpServlet {

    mCiudades mc = new mCiudades();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        

        response.setContentType("application/json;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String o = "err";
            String s = request.getSession().getId();
            if (!s.isEmpty()) {
                o = request.getParameter("o");
            }
            
            
            
            switch (o) {
                case "r":
                    out.print(mc.ciudades().toString());
                    break;
                case "s":
                    mc.setCiu_descri(request.getParameter("ciu_descri"));
                    out.print(mc.insertar());
                    break;
                case "m":
                    mc.setCiu_cod(Integer.parseInt(request.getParameter("ciu_cod")));
                    mc.setCiu_descri(request.getParameter("ciu_descri"));
                    out.print(mc.modificar());
                    break;
                case "e":
                    mc.setCiu_cod(Integer.parseInt(request.getParameter("ciu_cod")));
                    out.print(mc.eliminar());
                    break;
                case "err":
                    response.sendRedirect("/megaglobal_web_2/");
                    break;
            }
        }

    }


// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
/**
 * Handles the HTTP <code>GET</code> method.
 *
 * @param request servlet request
 * @param response servlet response
 * @throws ServletException if a servlet-specific error occurs
 * @throws IOException if an I/O error occurs
 */
@Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
        public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
