/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.mClientes;

/**
 *
 * @author damian
 */
@WebServlet(name = "clientes", urlPatterns = {"/clientes"})
public class clientes extends HttpServlet {

    mClientes mc = new mClientes();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");

        try (PrintWriter out = response.getWriter()) {
            String o = "err";
            String s = request.getSession().getId();
            if (!s.isEmpty()) {
                o = request.getParameter("o");
            }

            /**
             * cli_cod integer NOT NULL, cli_nombre character varying(60) NOT
             * NULL, cli_ci_ruc character varying(20) NOT NULL, cli_email
             * character varying(100) NOT NULL, cli_direccion character
             * varying(100) NOT NULL, ciu_cod integer NOT NULL,
             */
            String cli_cod = request.getParameter("cli_cod");
            String cli_nombre = request.getParameter("cli_nombre");
            String cli_ci_ruc = request.getParameter("cli_ci_ruc");
            String cli_email = request.getParameter("cli_email");
            String cli_direccion = request.getParameter("cli_direccion");
            String ciu_cod = request.getParameter("ciu_cod");
            String cli_telefono = request.getParameter("cli_telefono");

            switch (o) {
                case "c":
                    mc.setCli_nombre(cli_nombre);
                    mc.setCli_ci_ruc(cli_ci_ruc);
                    mc.setCli_direccion(cli_direccion);
                    mc.setCli_email(cli_email);
                    mc.setCiu_cod(Integer.parseInt(ciu_cod));
                    mc.setCli_telefono(cli_telefono);
                    out.print(mc.insertar());
                    break;
                case "r":
                    out.print(mc.clientes().toString());
                    break;
                case "u":
                    mc.setCli_cod(Integer.parseInt(cli_cod));
                    mc.setCli_nombre(cli_nombre);
                    mc.setCli_ci_ruc(cli_ci_ruc);
                    mc.setCli_direccion(cli_direccion);
                    mc.setCli_email(cli_email);
                    mc.setCiu_cod(Integer.parseInt(ciu_cod));
                    mc.setCli_telefono(cli_telefono);
                    out.print(mc.modificar());
                    break;
                
                case "d":
                    mc.setCli_cod(Integer.parseInt(cli_cod));
                    out.print(mc.eliminar());
                    break;
                case "ciudades":
                    out.print(mc.ciudades().toString());
                    break;
                    case "vr":
                        mc.setCli_ci_ruc(cli_ci_ruc);
                        out.print(mc.verifica_ruc().toString());
                        break;
                case "err":
                    response.sendRedirect("/megaglobal_web_2/");
                    break;
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
