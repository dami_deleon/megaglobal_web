/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.mLogin;
import modelo.mPropuestas;
import org.json.JSONArray;

/**
 *
 * @author Damián
 */
@WebServlet(name = "propuestas", urlPatterns = {"/propuestas"})
public class propuestas extends HttpServlet {

    mPropuestas mp = new mPropuestas();
    mLogin acceder = new mLogin();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String o = "err";
            String s = request.getSession().getId();
            if (!s.isEmpty()) {
                o = request.getParameter("o");
            }

            /**
             * private Integer pro_cod, emp_cod, cli_cod; private Boolean
             * pro_estado; private JSONArray detalle;
             */
            Integer emp_cod = acceder.usuario(s);
            String cli_cod = request.getParameter("cli_cod");
            String detalle = request.getParameter("detalle");
            switch (o) {
                case "b":
                    out.print(mp.servicios().toString());
                    break;
                case "r":
                    out.print(mp.propuestas().toString());
                    break;
                case "s": {
                    try {
                        JSONArray json = new JSONArray(detalle);
                        mp.setEmp_cod(emp_cod);
                        mp.setCli_cod(Integer.parseInt(cli_cod.trim()));
                        mp.setDetalle(json);
//                        mp.setToken(request.getParameter("token"));
//                        mp.createToken();
                        out.print(mp.nuevaPropuesta().toString());
                    } catch (SQLException ex) {
                        out.print("{\"error\":" + ex.getMessage() + "}");
                    }
                }

                break;
                case "m":
//                    mc.setCar_cod(Integer.parseInt(request.getParameter("car_cod")));
//                    mc.setCar_descri(request.getParameter("car_descri"));
//                    out.print(mc.modificar());
                    break;
                case "p":
                    mp.setPro_cod(Integer.parseInt(request.getParameter("pro_cod")));
                    out.print(mp.propuestaByCod());
                    break;
                case "clientes":
                    out.print(mp.clientes().toString());
                    break;

                case "err":
                    response.sendRedirect("/megaglobal_web_2/");
                    break;
            }
        } catch (Exception ex) {
            Logger.getLogger(propuestas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (!request.getSession().getAttribute("session").toString().isEmpty()) {
            processRequest(request, response);
        } else {
            try (PrintWriter out = response.getWriter()) {
                String o = "err";
            if (!request.getParameter("o").isEmpty()) {
                o = request.getParameter("o");
            }
            
            switch(o){
                case "v":
                    request.getParameter("token");
                    request.getParameter("pro_cod");
                break;
            }
            
            
            }
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
