/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.mLogin;
import org.json.JSONObject;
import generic.Mailer;

/**
 *
 * @author Damián
 */
public class login extends HttpServlet {

    mLogin acceder = new mLogin();
    Mailer msg = new Mailer();
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");

        response.sendRedirect("/megaglobal_web_2/");

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        try {
            HttpSession sesionActiva = request.getSession();
            String idsesion = sesionActiva.getId();

            if (request.getParameterMap().isEmpty()) {
                response.sendRedirect("/megaglobal_web_2/");
            }
            
            String user = request.getParameter("user");
            String pass = request.getParameter("password");

            acceder.setUsu_user(user);
            acceder.setUsu_password(pass);

            if (acceder.consultar()) {
                sesionActiva.setAttribute("session", idsesion);
                acceder.setUsu_session(idsesion);
                acceder.insertarData();
                json.put("acceso", true);
                json.put("txt", "Has iniciado sesion");
                //msg.enviar_mail(json);
                out.print(json.toString());
            } else {
                json.put("acceso", false);
                out.print(json.toString());
            }
        } catch (SQLException ex) {
            out.print("error" + ex.getMessage());
        }
    }

}
