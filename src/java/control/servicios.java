/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.mServicios;

/**
 *
 * @author damian
 */
@WebServlet(name = "servicios", urlPatterns = {"/servicios"})
public class servicios extends HttpServlet {

    mServicios ms = new mServicios();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String o = "err";
            if (request.isRequestedSessionIdValid()) {
                o = request.getParameter("o");
            }

            String ser_cod = request.getParameter("ser_cod");
            String ser_descripcion = request.getParameter("ser_descripcion");
            String ser_costo_mensual = request.getParameter("ser_costo_mensual");
            String ser_costo_unitario = request.getParameter("ser_costo_unitario");
            String med_cod = request.getParameter("med_cod");

            switch (o) {
                case "c":
                    ms.setSer_costo_mensual(Integer.parseInt(ser_costo_mensual.trim()));
                    ms.setSer_descripcion(ser_descripcion);
                    ms.setSer_costo_unitario(Integer.parseInt(ser_costo_unitario.trim()));
                    ms.setMed_cod(Integer.parseInt(med_cod.trim()));
                    out.print(ms.insertar());
                    break;
                case "r":
                    out.print(ms.servicios().toString());
                    break;
                case "u":
                    ms.setSer_cod(Integer.parseInt(ser_cod.trim()));
                    ms.setSer_costo_mensual(Integer.parseInt(ser_costo_mensual.trim()));
                    ms.setSer_descripcion(ser_descripcion);
                    ms.setSer_costo_unitario(Integer.parseInt(ser_costo_unitario.trim()));
                    ms.setMed_cod(Integer.parseInt(med_cod.trim()));
                    out.print(ms.modificar());
                    break;
                case "d":
                    ms.setSer_cod(Integer.parseInt(ser_cod.trim()));
                    out.print(ms.eliminar());
                    break;
                case "medios":
                    out.print(ms.medios().toString());
                    break;
                case "err":
                    response.sendRedirect("/megaglobal_web_2/");
                    break;
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
